<?php
App::uses('AppModel', 'Model');
/**
 * Order Model
 *
 * @property User $User
 * @property UserProfile $UserProfile
 * @property Truck $Truck
 * @property OrderProduct $OrderProduct
 */
class Order extends AppModel {

	public $displayField = 'site_name';

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'UserProfile' => array(
			'className' => 'UserProfile',
			'foreignKey' => 'user_profile_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Truck' => array(
			'className' => 'Truck',
			'foreignKey' => 'truck_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany = array(
		'OrderProduct' => array(
			'className' => 'OrderProduct',
			'foreignKey' => 'order_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
