<?php
App::uses('AppModel', 'Model');
/**
 * OrderProduct Model
 *
 * @property Order $Order
 */
class OrderProduct extends AppModel {

	public $displayField = 'label';

	public $belongsTo = array(
		'Order' => array(
			'className' => 'Order',
			'foreignKey' => 'order_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
