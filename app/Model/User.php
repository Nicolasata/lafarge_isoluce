<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class User extends AppModel {

	public $displayField = 'number';

	public function checkEmail($email) {
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return null;
		}
		
		return __('L\'adresse email est incorrecte.');
	}

	public function checkPwd($pwd, $pwdConfirm) {
		if(isset($pwd)) {
			$pwdLength = strlen($pwd);
			if($pwdLength >= 6) {
				if($pwd === $pwdConfirm) {
					return null;
				}
				else {
					return __('Le mot passe de confirmation n\'est pas identique.');
				}
			}
			else {
				return __('Le mot passe doit contenir au moins 6 caractères.');
			}
		}
		
		return __('Le mot passe est incorrect.');
	}

}
