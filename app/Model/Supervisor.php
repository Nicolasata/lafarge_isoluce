<?php
App::uses('AppModel', 'Model');
/**
 * Supervisor Model
 *
 */
class Supervisor extends AppModel {

	public $displayField = 'name';

}
