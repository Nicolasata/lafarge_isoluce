<?php
App::uses('AppModel', 'Model');

App::uses('SendinBlueInterface', 'Vendor');
App::uses('MessageType', 'Vendor/Constante');
App::uses('CustomException', 'Vendor/Constante');


/**
 * Message Model
 *
 * @property TeamUser $TeamUser
 */
class Message extends AppModel {

	public $displayField = 'template';

	public $belongsTo = array(
		'TeamUser' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function findToSend() {
		return $this->find('all', array(
			'conditions' => array(
				'Message.error' => 0,
				'Message.send' => null
			),
			'recursive' => -1
		));
	}

    public function setError($id, $error) {
        return $this->save(array(
			'Message' => array(
				'id' => $id,
				'error' => $error
			)
		));
	}
	
	public function sent($message) {
                
		switch($message['Message']['type']) {
			case MessageType::EMAIL:

				$params = json_decode($message['Message']['vars'], true);
			
				$SBinterface = new SendinBlueInterface(Configure::read('SendinBlue.api_key'));
				$SBinterface->sendTemplateEmail($message['Message']['template'], $message['Message']['to'], $params);

				break;
			default:
				throw new Exception('Message type not found', CustomException::MSG_TYPE_NOT_FOUND);
		}

		$now = new \DateTime('now', new \DateTimeZone('UTC'));
		
		$saveMessage = array(
			'Message' => array(
				'id' => $message['Message']['id'],
				'error' => 0,
				'send' => $now->format('Y-m-d H:i:s')
			)
		);     
		
		if($this->save($saveMessage)) {
			return true;
		}
		else {
			throw new Exception('Message send not saved', CustomException::MSG_ERROR_SAVE_SEND);
		}
	}

       
	public function addEmail($template, $to, $vars, $userId = null) {

		$message = array('Message' => array(
			'type' => MessageType::EMAIL,
			'template' => $template,
			'to' => $to,
			'user_id' => $userId,
			'vars' => json_encode($vars)
		));
		
		$this->create();
		if ($this->save($message)) {
			$message['Message']['id'] = $this->getLastInsertId();
			return $message;
		}
		
		throw new Exception('Message add error', CustomException::MSG_ADD_EMAIL);
	}
	
	public function addEmailAndSend($template, $to, $vars, $userId = null) {

		$message = $this->addEmail($template, $to, $vars, $userId);
		$this->sent($message);

		return true;
	}

}
