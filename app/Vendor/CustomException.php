<?php
App::uses('AppModel', 'Model');

class CustomException extends CakeException {

    const SB_REQUEST_FAILURE = 2000;
    const SB_FAIL_CONNEXION = 2010;  

    const SE_TOKEN_EXPIRED = 3000;
    const SE_TOKEN_INVALID = 3010;  
	
}