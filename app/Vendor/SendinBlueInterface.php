<?php
require_once('CustomException.php');

class SendinBlueInterface {

    private $baseUrl = 'https://api.sendinblue.com/v3/';
    private $apiKey = null;

    public function __construct($apiKey){
        $this->apiKey = $apiKey;
    }

    /**
     * Envoi un Template d'email par le mode Transactionnel
     * 
     * https://developers.sendinblue.com/v3.0/reference#sendtransacemail
     */
    public function sendTemplateEmail($templateId, $to, $attributes = array()) {
        
        $param = array( 
            "templateId" =>intval($templateId),
            "to" => array(
                array("email" => $to)
            ),
            //"cc" => "",
            //"bcc" => $bcc,
            //"replyto" => "",
            //"sender" => "",
            "params" => $attributes,
            //"attachment_url" => "",
            //"attachment" => array("myfilename.pdf" => "your_pdf_files_base64_encoded_chunk_data"),
            //"headers" => array("Content-Type"=> "text/html;charset=iso-8859-1", "X-param1"=> "value1")
        );
        
        $ch = curl_init($this->baseUrl.'smtp/email');

        $headers = array(
            "Accept: application/json",
            "Content-Type: application/json",
            "api-key: ".$this->apiKey
        );   

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            // Windows only over-ride
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($param));
        $data = curl_exec($ch);

        if(curl_errno($ch)) {
            throw new RuntimeException('cURL error: ' . curl_error($ch));
        }
        if(!is_string($data) || !strlen($data)) {
            throw new RuntimeException('Request Failed');
        }
        curl_close($ch);

        $data = json_decode($data,true);

        if(isset($data['messageId']) && !empty($data['messageId'])){
            return $data;
        }
        else{
            throw new CustomException('SendinBlueInterface/sendTemplateEmail::'.json_encode($data), CustomException::SB_REQUEST_FAILURE);
        }
    }

    
    /**
     * Ajout un email à une liste
     * 
     */
    public function addEmailToList($email, $listId) {

        $email = trim(strtolower($email));

        //Params
        $headers = array(
            "Accept: application/json",
            "Content-Type: application/json",
            "api-key: ".$this->apiKey
        ); 

        //Test if user exist
        $chTestUser = curl_init($this->baseUrl.'contacts/'.$email);
  
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            // Windows only over-ride
            curl_setopt($chTestUser, CURLOPT_SSL_VERIFYPEER, false);
        }

        curl_setopt($chTestUser, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($chTestUser, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chTestUser, CURLOPT_ENCODING, "");
        curl_setopt($chTestUser, CURLOPT_MAXREDIRS, 10);
        curl_setopt($chTestUser, CURLOPT_TIMEOUT, 30);
        curl_setopt($chTestUser, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($chTestUser, CURLOPT_CUSTOMREQUEST, 'GET');
        $dataTestUser = curl_exec($chTestUser);

        if(curl_errno($chTestUser)) {
            throw new RuntimeException('cURL error: ' . curl_error($chTestUser));
        }
        if(!is_string($dataTestUser) || !strlen($dataTestUser)) {
            throw new RuntimeException('Request Failed');
        }
        curl_close($chTestUser);

        $dataTestUser = json_decode($dataTestUser,true);

        if(!isset($dataTestUser['id']) || empty($dataTestUser['id'])) {

            //Create contact

            $param = array( 
                "email" => $email,
                'listIds' => array(
                    $listId
                )
            );

            $chCreateUser = curl_init($this->baseUrl.'contacts/');

            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                // Windows only over-ride
                curl_setopt($chCreateUser, CURLOPT_SSL_VERIFYPEER, false);
            }

            curl_setopt($chCreateUser, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($chCreateUser, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($chCreateUser, CURLOPT_ENCODING, "");
            curl_setopt($chCreateUser, CURLOPT_MAXREDIRS, 10);
            curl_setopt($chCreateUser, CURLOPT_TIMEOUT, 30);
            curl_setopt($chCreateUser, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1); 
            curl_setopt($chCreateUser, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($chCreateUser, CURLOPT_POSTFIELDS, json_encode($param));
            $dataCreateUser = curl_exec($chCreateUser);

            if(curl_errno($chCreateUser)) {
                throw new RuntimeException('cURL error: ' . curl_error($chCreateUser));
            }
            if(!is_string($dataCreateUser) || !strlen($dataCreateUser)) {
                throw new RuntimeException('Request Failed');
            }
            curl_close($chCreateUser);

            $dataCreateUser = json_decode($dataCreateUser,true);

            if(!isset($dataCreateUser['id']) || empty($dataCreateUser['id'])){
                throw new CustomException('SendinBlueInterface/addEmailToList::'.json_encode($dataCreateUser), CustomException::SB_REQUEST_FAILURE);
            }

        }

        
        //Add contact to list

        $param = array( 
            "emails" => array(
                $email
            )
        );

        $chUpdateUser = curl_init($this->baseUrl.'contacts/lists/'.$listId.'/contacts/add');

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            // Windows only over-ride
            curl_setopt($chUpdateUser, CURLOPT_SSL_VERIFYPEER, false);
        }

        curl_setopt($chUpdateUser, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($chUpdateUser, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chUpdateUser, CURLOPT_ENCODING, "");
        curl_setopt($chUpdateUser, CURLOPT_MAXREDIRS, 10);
        curl_setopt($chUpdateUser, CURLOPT_TIMEOUT, 30);
        curl_setopt($chUpdateUser, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1); 
        curl_setopt($chUpdateUser, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($chUpdateUser, CURLOPT_POSTFIELDS, json_encode($param));
        $dataUpdateUser = curl_exec($chUpdateUser);

        if(curl_errno($chUpdateUser)) {
            throw new RuntimeException('cURL error: ' . curl_error($chUpdateUser));
        }
        if(!is_string($dataUpdateUser) || !strlen($dataUpdateUser)) {
            throw new RuntimeException('Request Failed');
        }
        curl_close($chUpdateUser);

        $dataUpdateUser = json_decode($dataUpdateUser,true);

        if(isset($dataUpdateUser['contacts']) && isset($dataUpdateUser['contacts']['success']) && count($dataUpdateUser['contacts']['success']) > 0){
            return true;
        }
        else {
            throw new CustomException('SendinBlueInterface/addEmailToList::'.json_encode($dataUpdateUser), CustomException::SB_REQUEST_FAILURE);
        }
        
    }

}