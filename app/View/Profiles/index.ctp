<?php 
	$this->start('title');
	echo __('Profils');
	$this->end(); 

?>


        <div id="login-container">
            <h1 class="h2 text-light text-center push-top-bottom animation-pullDown">
                <img src="<?php echo Router::url('/', true); ?>img/logo_lafarge.png" class="logo-login">
            </h1>
            <h1 class="text-light text-center push-top-bottom animation-pullDown text-black">
                Sélection du profil
            </h1>
            
        </div>
        <div id="login-container" class="profile-container">
            
            <div class="row">
                <?php 
                if(count($profiles)) {
                    foreach($profiles as $profile) {
                        ?>
                        <div class="profil col-xs-12 col-sm-6 col-md-3 margin-bottom-20-mobile">
                             <a href="<?php echo Router::url('/', true); ?>profiles/remove/<?php echo $profile['UserProfile']['id']; ?>" class="delete-profil smooth-transition"><i class="fa fa-trash"></i></a>
                             <a href="<?php echo Router::url('/', true); ?>profiles/select/<?php echo $profile['UserProfile']['id']; ?>" class="select-profile">
                                <div class="widget-content border-bottom text-center themed-background-profile">
                                    <div class="widget-content text-center padding-0">
                                        <div class="widget-icon login-profile">
                                        <?php echo strtoupper(substr(trim($profile['UserProfile']['firstname']), 0, 1)); ?>
                                        </div>
                                    </div>
                                    <h2 class="widget-heading h3 text-black"><?php echo $profile['UserProfile']['firstname']; ?><br /><?php echo $profile['UserProfile']['lastname']; ?></h2>
                                </div>
                            </a>
                        </div>
                        <?php
                    }
                }
                ?>
                
                <div class="profil col-xs-12 col-sm-6 col-md-3 margin-bottom-20-mobile" style="margin-top: 35px; <?php echo (count($profiles)) ? 'opacity:0.6;' : ''; ?>">
                    <a href="<?php echo Router::url('/', true); ?>profiles/add" class="select-profile">
                        <div class="widget-content border-bottom text-center themed-background-profile">
                            <div class="widget-content text-center padding-0">
                                <div class="widget-icon login-profile add-login-profile">
                                    +
                                </div>
                            </div>
                            <h2 class="widget-heading h3 text-black">Ajouter<br />un profil</h2>
                        </div>
                    </a>
                </div>
                
            </div>
        </div>

<?php
echo $this->Html->css('user.profilindex');