<?php 
	$this->start('title');
	echo __('Ajouter un profil');
	$this->end(); 

?>

        <div id="login-container">
            <h1 class="h2 text-light text-center push-top-bottom animation-pullDown">
                <img src="<?php echo Router::url('/', true); ?>img/logo_lafarge.png" class="logo-login">
            </h1>
            <h1 class="text-light text-center push-top-bottom animation-pullDown text-black">
            Ajouter un profil
            </h1>
        </div>

        <div id="login-container" class="profile-container">
            <div class="block animation-fadeInQuick border-lafarge">
                <?php echo $this->Form->create(false, array('url' => array('controller' => 'profiles', 'action' => 'add'), 'class' => 'form-horizontal', 'id' => 'form-add', 'novalidate' => 'novalidate')); ?>
               
                    <div class="form-group">
                       <div class="">
                          <div class="col-xs-12 col-sm-6 margin-bottom-20-mobile">
                                <label for="lastname" class="col-xs-12">Nom</label>
                                <div class="col-xs-12 input-text-name">
                                    <input type="text" id="lastname" name="lastname" class="form-control" placeholder="">
                                </div>
                          </div>
                          <div class="col-xs-12 col-sm-6">
                                <label for="firstname" class="col-xs-12 input-text-lastname">Prénom</label>
                                <div class="col-xs-12 form-text-lastname">
                                    <input type="text" id="firstname" name="firstname" class="form-control" placeholder="">
                                </div>
                          </div>  
                       </div>
                    </div>
                    
                    <div class="form-group">
                       <div class="">
                            <div class="col-xs-12 col-sm-4 margin-bottom-20-mobile">
                                <label for="phone" class="col-xs-12 form-text-phone">Téléphone</label>
                                <div class="col-xs-12 input-text-phone">
                                    <input type="text" id="phone" name="phone" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 margin-bottom-20-mobile">
                                <label for="work" class="col-xs-12 form-text-fonction">Fonction</label>
                                <div class="col-xs-12 input-text-fonction">
                                    <input type="text" id="work" name="work" class="form-control" placeholder="">
                                </div>
                            </div>  
                            <div class="col-xs-12 col-sm-4">
                                <label for="email" class="col-xs-12 form-text-email">Email</label>
                                <div class="col-xs-12 input-text-email">
                                    <input type="text" id="email" name="email" class="form-control" placeholder="">
                                </div>
                            </div>
                       </div>
                    </div>

                    <div class="form-group" style="margin:20px 0px;">
                        <div class="">
                            <div class="col-xs-8">
                                <a href="<?php echo Router::url('/', true); ?>profiles" title="Retour" class="text-specific">Retour</a>
                            </div>
                            <div class="col-xs-4 text-right">
                                <button type="submit" class="btn btn-effect-ripple btn-sm btn-success">Valider</button>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
        
<?php
echo $this->Html->css('user.profilindex');
echo $this->Html->css('user.profiladd');