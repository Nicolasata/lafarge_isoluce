<?php 
	$this->start('title');
	echo __('Commande acceptée');
	$this->end(); 

?>

    <div id="login-container">
        <h1 class="h2 text-light text-center push-top-bottom animation-pullDown">
            <img src="<?php echo Router::url('/', true); ?>img/logo_lafarge.png" class="logo-login">
        </h1>
        <h1 class="text-light text-center push-top-bottom animation-pullDown text-black">
            Traitement de la commande
        </h1>
        <?php 
        if(isset($error) && $error != null) {
            ?>
            <div class="alert alert-danger text-center">
                <?php echo $error; ?>
            </div>
            <?php
        }
        else {
            ?>
            <div class="alert alert-success text-center">
                Le traitement de cette commande est enregistrée. Un email vient d'être envoyer au client.<br />
                Traitement choisi : Accepté
            </div>
            <?php
        }
    ?>
    </div>        
        
<?php

