<?php 
	$this->start('title');
	echo __('Passer une commande');
	$this->end(); 

?>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <div class="block">
                <div class="block-title">
                    <h2>Passer une commande<span class="title-date" style="display:none;"> pour le <span class="title-date-value"></span></span> :</h2>
                </div>

                <div class="" id="" style="margin: 0 15px 15px;">
                    <?php echo $this->Form->create('Order', array(
                        'class' => 'form-horizontal',
                        'url' => 'javascript:void(0);',
                        'id' => 'OrderAddForm',
                        'enctype' => 'multipart/form-data',
                        'type' => 'POST'
                    )); 
                    ?>

                        <div class="row no-margin-row">
                            <div id="step-1" class="step">
                                <?php echo $this->element('Orders/add_breadcrumb', array('step' => 1)); ?>
                                <?php echo $this->element('Orders/add_step1'); ?>
                            </div>

                            <div id="step-2" class="step">
                                <?php echo $this->element('Orders/add_breadcrumb', array('step' => 2)); ?>
                                <?php echo $this->element('Orders/add_step2'); ?>
                            </div>

                            <div id="step-3" class="step">
                                <?php echo $this->element('Orders/add_breadcrumb', array('step' => 3)); ?>
                                <?php echo $this->element('Orders/add_step3'); ?>
                            </div>

                            <div id="step-4" class="step">
                                <?php echo $this->element('Orders/add_breadcrumb', array('step' => 4)); ?>
                                <?php echo $this->element('Orders/add_step4'); ?>
                            </div>

                            <div id="step-5" class="step">
                                <?php echo $this->element('Orders/add_breadcrumb', array('step' => 5)); ?>
                                <?php echo $this->element('Orders/add_step5'); ?>
                            </div>

                            <div id="step-6" class="step">
                                <?php echo $this->element('Orders/add_breadcrumb', array('step' => 6)); ?>
                                <?php echo $this->element('Orders/add_step6'); ?>
                            </div>
                        </div>

                        <div class="row no-margin-row margin-bottom-20 margin-top-20">
                            <div class="form-group form-actions text-center">
                                <div class="col-xs-12">
                                    <button type="button" class="btn btn-effect-ripple btn-danger" id="wizard-button-previous" style="display:none; margin-right:10px;"><?php echo __('Précédent'); ?></button>
                                    <button type="button" class="btn btn-effect-ripple btn-specific" id="wizard-button-next"><span id="wizard-button-next-text"><?php echo __('Valider'); ?></span></button>
                                </div>
                            </div>
                        </div>
                        
                    <?php echo $this->Form->end(); ?>  

                </div>

            </div>
        </div>
    </div>

<script>

    var rootUrl = '<?php echo Router::url('/', true); ?>';
    
    var currentStep = <?php echo isset($finish) && $finish ? 6 : 1; ?>;

    var mapKey = '<?php echo Configure::read('GoogleMap.KeyApi'); ?>';
    var geocoder;
    var stonepits = JSON.parse('<?php echo  str_replace("'", "\'", json_encode($stonepits)); ?>');
    
    $(function(){ 
        
        FormsWizard.init(); 
        
        var s = document.createElement('script'), h = document.head;
        s.async = true;
        s.src = 'https://maps.googleapis.com/maps/api/js?key=' + mapKey + '&callback=mapLoaded&libraries=geometry';
        h.appendChild(s);
    
    });

</script>

<?php
echo $this->Html->css('orders.add');
echo $this->Html->script('orders.add');