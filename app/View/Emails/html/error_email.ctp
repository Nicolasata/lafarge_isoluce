<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Emails.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

<h2><?php echo $Info; ?></h2>


<?php
foreach ($Logs as $log):
    
        echo "\n__________________________________\n";
    
    ?>
        <h3><?php echo ('Resume');?></h3>
    <?php
    
        echo '<p> ' . $log->getMessage() . "</p>\n";
    
    ?>
        <h3><?php echo ('StackTrace');?></h3>
    <?php
    
        echo '<p> ' . $log->getTraceAsString() . "</p>\n";
        
        echo "\n__________________________________\n";
        
        
endforeach;
?>