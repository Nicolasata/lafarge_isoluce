<?php 
	$this->start('title');
	echo ('Exporter les commandes');
	$this->end(); 

?>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <div class="block">
                <div class="block-title">
                    <h2>Exporter les commandes</h2>
                </div>

                <div class="row" style="margin:15px 0;">

                    <?php 
                    if(isset($error) && $error != null) {
                        ?>
                        <div class="margin-top-20 margin-bottom-20 alert alert-danger">  
                            <?php echo $error; ?>
                        </div>
                        <?php 
                    }
                    ?>

                    <?php echo $this->Form->create(false, array(
                        'class' => 'form-horizontal',
                        'url' => array('controller' => 'supervisors', 'action' => 'export'),
                        'type' => 'POST'
                    )); ?>
                    
                        <?php
                            $dateStart = new DateTime();
                            $dateStart->sub(new DateInterval('P1M'));
                            $dateEnd = new DateTime();
                        ?>
                    
                        <div class="row margin-top-20 margin-bottom-20">  
                            <div class="col-xs-12 col-md-6">                      
                                <div class="width-100 text-left">
                                    <label for="ExportDateStart">Date de début</label>
                                </div>
                                <div class="input-group"> 
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" id="ExportDateStart" name="data[Export][date_start]" class="form-control input-datepicker width-100" data-date-format="dd/mm/yyyy" placeholder="jj/mm/yyyy" value="<?php echo $dateStart->format('d/m/Y'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">                      
                                <div class="width-100 text-left">
                                    <label for="ExportDateEnd">Date de fin</label>
                                </div>
                                <div class="input-group"> 
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" id="ExportDateEnd" name="data[Export][date_end]" class="form-control input-datepicker width-100" data-date-format="dd/mm/yyyy" placeholder="jj/mm/yyyy" value="<?php echo $dateEnd->format('d/m/Y'); ?>">
                                </div>
                            </div>
                            <div class="col-sm-12 margin-btn text-right margin-top-20">
                                <button type="submit" class="btn btn-effect-ripple btn-success" style="overflow: hidden; position: relative;">Générer</button>
                            </div> 
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>

            </div>
        </div>
    </div>
