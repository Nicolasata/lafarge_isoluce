<?php 
	$this->start('title');
	echo ('Importation');
	$this->end(); 

?>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <div class="block">
                <div class="block-title">
                    <h2>Importer les clients</h2>
                </div>

                <div class="row" style="margin:15px 0;">

                    <?php 
                    if(isset($errorClients) && $errorClients != null) {
                        ?>
                        <div class="margin-top-20 margin-bottom-20 alert alert-danger">  
                            <?php echo $errorClients; ?>
                        </div>
                        <?php 
                    }
                    ?>

                    <?php echo $this->Form->create(false, array(
                        'class' => 'form-horizontal',
                        'url' => array('controller' => 'supervisors', 'action' => 'imports'),
                        'enctype' => 'multipart/form-data',
                        'type' => 'POST'
                    )); ?>
                        <input type="hidden" name="type" value="clients" />   
                        <div class="row margin-top-20 margin-bottom-20">  
                            <div class="col-xs-12">
                                <label class="control-label" for="file_clients" style="text-align:left; padding-left:0px; margin-bottom:5px;">Fichier Clients (.xls) :</label>
                                <input type="file" id="file_clients" name="data[Import][file_clients]" />
                            </div>
                            <div class="col-sm-12 margin-btn text-right margin-top-20">
                                <button type="submit" class="btn btn-effect-ripple btn-success" style="overflow: hidden; position: relative;">Importer</button>
                            </div> 
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>

            </div>
        </div>

        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <div class="block">
                <div class="block-title">
                    <h2>Importer les camions</h2>
                </div>

                <div class="row" style="margin:15px 0;">

                    <?php 
                    if(isset($errorTrucks) && $errorTrucks != null) {
                        ?>
                        <div class="margin-top-20 margin-bottom-20 alert alert-danger">  
                            <?php echo $errorTrucks; ?>
                        </div>
                        <?php 
                    }
                    ?>

                    <?php echo $this->Form->create(false, array(
                        'class' => 'form-horizontal',
                        'url' => array('controller' => 'supervisors', 'action' => 'imports'),
                        'enctype' => 'multipart/form-data',
                        'type' => 'POST'
                    )); ?>
                        <input type="hidden" name="type" value="trucks" />   
                        <div class="row margin-top-20 margin-bottom-20">  
                            <div class="col-xs-12">
                                <label class="control-label" for="file_trucks" style="text-align:left; padding-left:0px; margin-bottom:5px;">Fichier Camions (.xls) :</label>
                                <input type="file" id="file_trucks" name="data[Import][file_trucks]" />
                            </div>
                            <div class="col-sm-12 margin-btn text-right margin-top-20">
                                <button type="submit" class="btn btn-effect-ripple btn-success" style="overflow: hidden; position: relative;">Importer</button>
                            </div> 
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>

            </div>
        </div>
    </div>
