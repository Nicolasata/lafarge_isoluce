<?php


    $matching = array(
        'id'                  =>  array('Order' => 'id'),
        'created'                  =>  array('Order' => 'created'),
        'profile_lastname'                  =>  array('UserProfile' => 'lastname'),
        'profile_firstname'                  =>  array('UserProfile' => 'firstname'),
        'profile_email'                  =>  array('UserProfile' => 'email'),
        'profile_phone'                  =>  array('UserProfile' => 'phone'),
        'site_name'                  =>  array('Order' => 'site_name'),
        'site_addr'                  =>  array('Order' => 'site_addr'),
        'site_lat'                  =>  array('Order' => 'site_lat'),
        'site_lng'                  =>  array('Order' => 'site_lng'),
        'order_number'                  =>  array('Order' => 'order_number'),
        'order_price_linked'        =>  array('Order' => 'order_price_linked'),
        'products'                  =>  array('OrderProduct'),
        'delivery_date'                  =>  array('Order' => 'order_date'),
        'delivery_range'                  =>  array('Order' => 'delivery_range'),
        'delivery_comments'                  =>  array('Order' => 'delivery_comments'),        
        'truck'                  =>  array('Truck' => 'libelle_court'), 
        'cuttings_return'                  =>  array('Order' => 'cuttings_return'),
        'send'                  =>  array('Order' => 'send'),
        'accepted'                  =>  array('Order' => 'validated'),
        'updated'                  =>  array('Order' => 'refused'),
        'comments'                  =>  array('Order' => 'order_comments'),
        'file_order'                  =>  array('Order' => 'order_file'),
        'site_access'                  =>  array('Order' => 'site_file_access'),
    );
    
    
    $keys = array_keys($matching);
    
    $line = $keys;
    $this->CSV->addRow($line);
    
    foreach ($orders as $order)
    {
        
        $line = array();

        $order['Order']['delivery_range'] = implode(' / ', json_decode($order['Order']['delivery_range'], true));
        
        $order['Order']['cuttings_return'] = ($order['Order']['cuttings_return'] == '1' ? 'Oui' : 'Non');
            
        foreach ($keys as $key) {
            $match = $matching[$key];
            $value = null;
            
            if(is_array($match)) {
                if(key($match) == 'OrderProduct') {
                    
                    $productsArray = array();
                    foreach($order['OrderProduct'] as $p) {
                        array_push($productsArray, $p['quantity']. ' ' .$p['label']);
                    }
                    $value = implode(' / ', $productsArray);
                }
                else if(current($match) == 'order_file' || current($match) == 'site_file_access') {
                    if($order[key($match)][current($match)] != null) {
                        $value = Router::url('/', true).$order[key($match)][current($match)];
                    }
                    else {
                        $value = '';
                    }
                }
                else {
                    $value = $order[key($match)][current($match)];
                }
            }
            
            if($value != null) {
                $line[$key] = $value;
            }
            else {
                $line[$key] = '';
            }
        }
        
        $this->CSV->addRow($line);
    }
    
    echo  $this->CSV->render($filename, "UTF-8");
