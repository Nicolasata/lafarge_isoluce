<?php 
	$this->start('title');
	echo __('Récupération du mot de passe');
	$this->end(); 

?>

    <div id="login-container">
        <h1 class="h2 text-light text-center push-top-bottom animation-pullDown">
            <img src="<?php echo Router::url('/', true); ?>img/logo_lafarge.png" class="logo-login">
        </h1>
        <h1 class="text-light text-center push-top-bottom animation-pullDown text-black">
            Récupération du mot de passe
        </h1>

        <div class="block animation-fadeInQuick border-lafarge">
            <?php echo $this->Form->create('User', array('url' => array('action' => 'recoverPassword'), 'class' => 'form-horizontal', 'id' => 'form-recover-password', 'novalidate' => 'novalidate')); ?>
                <input type="hidden" name="id" value="<?php echo $i; ?>"/>
                <input type="hidden" name="hash" value="<?php echo $h; ?>"/>
                <div class="form-group">
                    <label for="pwd1" class="col-xs-12">Nouveau mot de passe</label>
                    <div class="col-xs-12">
                        <input type="password" id="pwd1" name="pwd1" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="pwd2" class="col-xs-12">Confirmation mot de passe</label>
                    <div class="col-xs-12">
                        <input type="password" id="pwd2" name="pwd2" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-xs-12 text-right">
                        <button type="submit" class="btn btn-effect-ripple btn-sm btn-success">Valider</button>
                    </div>
                </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
        
<?php
echo $this->Html->css('user.forgot_password');