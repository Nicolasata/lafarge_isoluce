<?php 
	$this->start('title');
	echo __('Mot de passe perdu');
	$this->end(); 

?>

    <div id="login-container">
        <h1 class="h2 text-light text-center push-top-bottom animation-pullDown">
            <img src="<?php echo Router::url('/', true); ?>img/logo_lafarge.png" class="logo-login">
        </h1>
        <h1 class="text-light text-center push-top-bottom animation-pullDown text-black">
            Mot de passe perdu
        </h1>
        <div class="block animation-fadeInQuick border-lafarge">
            <?php echo $this->Form->create('User', array('url' => array('action' => 'forgotPassword'), 'class' => 'form-horizontal', 'id' => 'form-forgot-password', 'novalidate' => 'novalidate')); ?>
                <div class="form-group">
                    <label for="login-email" class="col-xs-12">Email</label>
                    <div class="col-xs-12">
                        <input type="text" id="email" name="email" class="form-control" placeholder="Votre email..">
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-xs-8">
                        <a href="<?php echo Router::url('/', true); ?>" title="Récupération du mot de passe" class="text-specific">Retour à l'accueil</a>
                    </div>
                    <div class="col-xs-4 text-right">
                        <button type="submit" class="btn btn-effect-ripple btn-sm btn-success">Valider</button>
                    </div>
                </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
        
<?php
echo $this->Html->css('user.forgot_password');