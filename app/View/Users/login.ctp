<?php 
	$this->start('title');
	echo __('Connexion');
	$this->end(); 

?>

    <div id="login-container">
        <h1 class="h2 text-light text-center push-top-bottom animation-pullDown">
            <img src="<?php echo Router::url('/', true); ?>img/logo_lafarge.png" class="logo-login">
        </h1>
        <h1 class="text-light text-center push-top-bottom animation-pullDown text-black">
            Connexion
        </h1>
        <div class="block animation-fadeInQuick border-lafarge">
            <?php echo $this->Form->create('User', array('url' => array('action' => 'login'), 'class' => 'form-horizontal', 'id' => 'form-login', 'novalidate' => 'novalidate')); ?>
                <div class="form-group">
                    <label for="login-number" class="col-xs-12">Numéro de compte client</label>
                    <div class="col-xs-12">
                        <input type="text" id="login-number" name="number" class="form-control" placeholder="Votre numéro">
                    </div>
                </div>
                <div class="form-group">
                    <label for="login-password" class="col-xs-12">Mot de passe</label>
                    <div class="col-xs-12">
                        <input type="password" id="login-password" name="password" class="form-control" placeholder="Votre mot de passe..">
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-xs-8">
                        <a href="<?php echo Router::url('/', true); ?>forgot-password" title="Récupération du mot de passe" class="text-specific">Mon mot de passe oublié</a>
                    </div>
                    <div class="col-xs-4 text-right">
                        <button type="submit" class="btn btn-effect-ripple btn-sm btn-success text-white">Connexion</button>
                    </div>
                </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
        
        
<?php
echo $this->Html->css('user.login');

