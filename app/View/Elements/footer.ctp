<div class="navbar navbar-specific bar-footer padding-top-20 padding-bottom-20 margin-bottom-0" style="background: #454e59;">
    <div class="row no-margin">
        <div class="col-sm-12 text-center t15 margin-footer" id="footer-bar">
            <span class="white-text hidden-xs"> Pour toutes questions merci de nous contacter au <a href="tel:+33158006000" class="text-white">+33 1 58 00 60 00</a> ou sur <a href="mailto:logistiquebenne.sudest@lafargeholcim.com" class="text-white">logistiquebenne.sudest@lafargeholcim.com</a></span>
        </div>
        <div class="col-sm-12 text-center t15 margin-footer" id="footer-bar">
            <span class="white-text hidden-xs"> Une solution sur mesure développée par <a href="https://isoluce.net/" class="text-white " target="blank"> <img style="max-height: 30px; padding-bottom: 9px;" src="<?php echo Router::url('/', true); ?>img/logo_isoluce.png" alt="Logo iSoluce" /></a> </span>
        </div>
    </div>
</div>