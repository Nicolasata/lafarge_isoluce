<?php

if(!$this->Session->check('Supervisor')) {

    if(null != Configure::read('Common.ga')) {
    ?>
    <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
        ga('create', '<?php echo Configure::read('Common.ga'); ?>', 'auto');
        ga('send', 'pageview');
    </script>
    <?php
    }
    
    if(null != Configure::read('Common.smartlook')) {
    ?>
    <script type="text/javascript">
        window.smartlook||(function(d) {
        var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
        var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
        c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
        })(document);
        smartlook('init', '<?php echo Configure::read('Common.smartlook'); ?>');
    </script>
    <?php
    }
    
    if(null != Configure::read('Common.hotjar')) {
    ?>
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:<?php echo Configure::read('Common.hotjar'); ?>,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <?php
    }

    
    if(null != Configure::read('Common.linkedin_ads')) {
    ?>
    
    <script type="text/javascript">
    _linkedin_partner_id = "<?php echo  Configure::read('Common.linkedin_ads'); ?>";
    window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
    window._linkedin_data_partner_ids.push(_linkedin_partner_id);
    </script><script type="text/javascript">
    (function(){var s = document.getElementsByTagName("script")[0];
    var b = document.createElement("script");
    b.type = "text/javascript";b.async = true;
    b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
    s.parentNode.insertBefore(b, s);})();
    </script>
    <noscript>
    <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=<?php echo  Configure::read('Common.linkedin_ads'); ?>&fmt=gif" />
    </noscript>

    <?php
    }
    
}

?>

<script type="text/javascript">
    function sendGAEvent(category, action, label = 'undefined') {
        console.log('sendGAEvent (category:'+category+' action:'+action+' label:'+label+')');
        if(typeof(ga) !== 'undefined') {
            if(label !== 'undefined') {
                ga('send', 'event', category, action, label);
            } 
            else {
                ga('send', 'event', category, action);
            }
        }
    }
</script>

