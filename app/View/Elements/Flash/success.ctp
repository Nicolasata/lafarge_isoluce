<script type="text/javascript">
    $(document).ready(function () {
        $.bootstrapGrowl('<h4><strong>Notification</strong></h4> <p><?php echo h($message) ?></p>', {
            type: "success",
            delay: 2500,
            allow_dismiss: true,
            offset: {from: 'top', amount: 80}
        });
    });
</script>