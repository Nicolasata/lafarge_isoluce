<script type="text/javascript">
    $(document).ready(function () {
        $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p><?php echo h($message) ?></p>', {
            type: "danger",
            delay: 10000,
            allow_dismiss: true,
            offset: {from: 'top', amount: 80}
        });
    });
</script>