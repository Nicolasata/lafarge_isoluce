
    <div class="row no-margin-row">
        
        <label class="col-md-12 control-label text-center margin-top-20 margin-bottom-20" for="example-clickable-pc" style="font-style:italic; width: 100%; margin: auto; text-align: center; margin-top: 0px !important;"><strong>Renseigner l'adresse du chantier et précisée la, en déplaçant le point sur la carte</strong> </label>
        
        <div class="form-group">
            <div class="col-md-4">
                
                <label class="control-label" for="site_name" style="text-align:left; padding-left:0px; margin-bottom:5px;">Nom du chantier :</label>
                <input type="text" id="site_name" name="data[Order][site_name]" class="form-control" placeholder="Nom..">
            </div>
            <div class="col-md-5">
                <label class="control-label" for="site_addr" style="text-align:left; padding-left:0px; margin-bottom:5px;">Adresse du chantier :</label>
                <input type="text" id="site_addr" name="data[Order][site_addr]" class="form-control" placeholder="10 chemin de..">
                <input type="hidden" id="site_lat" name="data[Order][site_lat]" value="">
                <input type="hidden" id="site_lng" name="data[Order][site_lng]" value="">
            </div>
            <div class="col-md-3">
                <label class="control-label" for="order_price_linked" style="text-align:left; padding-left:0px; margin-bottom:5px;">Offre de prix liée</label>
                <input type="text" id="order_price_linked" name="data[Order][order_price_linked]" class="form-control" placeholder="Ecrire Oui ou Non.">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-4">
                <label class="control-label" for="name_contact" style="text-align:left; padding-left:0px; margin-bottom:5px;">Nom du contact chantier:</label>
                <input type="text" id="name_contact_site" name="data[Order][name_contact_site]" class="form-control" placeholder="Nom..">
            </div>
            <div class="col-md-5">
                <label class="control-label" for="surname_contact" style="text-align:left; padding-left:0px; margin-bottom:5px;">Prénom du contact chantier :</label>
                <input type="text" id="firstname_contact_site" name="data[Order][firstname_contact_site]" class="form-control" placeholder="Prénom..">
            </div>
            <div class="col-md-3">
                <label class="control-label" for="phonenbr_contact" style="text-align:left; padding-left:0px; margin-bottom:5px;">Numéro de téléphone :</label>
                <input type="text" id="phonenbr_contact_site" name="data[Order][phonenbr_contact_site]" class="form-control" placeholder="EX: 0665431278">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12"> 
                <div id="map_step1"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <label class="control-label" for="order_number" style="text-align:left; padding-left:0px; margin-bottom:5px;">Votre numéro de bon de commande  à rappeler dans le chantier, si obligatoire :</label>
                <input type="text" id="order_number" name="data[Order][order_number]" class="form-control" placeholder="12345..">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6">
                <label class="control-label" for="order_file" style="text-align:left; padding-left:0px; margin-bottom:5px;">Bon de commande :</label>
                <input type="file" id="order_file" name="data[Order][order_file]" />
            </div>
            <div class="col-md-6">
                <label class="control-label" for="site_file_access" style="text-align:left; padding-left:0px; margin-bottom:5px;">Plan d'accès : (facultatif)</label>
                <input type="file" id="site_file_access" name="data[Order][site_file_access]" />
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-6" >Dérogation : </label>
                <div class="col-md-12">
                <label class="switch switch-success">
                    <input type="checkbox" id="switch-derogation"><span></span><input>
                </label>
                </div>

                <div class="col-md-6" id ="upload-derogation" style="display:none">
                    <label class="control-label" style=" text-align:left; padding-left:0px; margin-bottom:5px;"></label>
                    <input type="file" id="derogation_file" name="data[Order][derogation_file]" />
                </div>

            </div>
        </div>

        <div class="form-group">
            <label class="col-md-12" for="">Risques chantier (pylones électriques, école, etc) : </label>
            <div class="col-md-12">
                <textarea id="risk_comments" name="data[Order][risk_comments]" rows="4" class="form-control width-100 margin-top-5" placeholder="Vous pouvez ajouter un commentaire concernant les risques liés chantier"></textarea>
            </div>
        </div>

    </div>
        
    <script>
        
        var i18n_position_find_invalid = '<?php echo __('Aucun position trouvée avec cette adresse.'); ?>';
        
        var mapInfoStep1 = {};
        var mapStep1;
        var markerStep1;

        jQuery(function($){

            mapInfoStep1.id = 'map_step1';
            mapInfoStep1.zoom = 6;
            mapInfoStep1.lat = 45.291262;
            mapInfoStep1.lng = 2.821910;

        });

    </script>

