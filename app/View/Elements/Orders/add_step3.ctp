            


    <div class="row no-margin-row">
        
        <div class="block-camions-rotation" style="display:none;">
            <div class="form-group">
                <label class="col-md-5 control-label" for="order_truck_number">Nombre de camions :</label>
                <div class="col-md-5">
                    <input type="number" id="order_truck_number" name="data[Order][order_truck_number]" class="form-control" placeholder="Nombre de camions">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-5 control-label" for="order_truck_volume_day">Volume journalier :</label>
                <div class="col-md-5">
                    <input type="text" id="order_truck_volume_day" name="data[Order][order_truck_volume_day]" class="form-control" placeholder="Volume journalier">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-5 control-label" for="order_total">Total :</label>
                <div class="col-md-5">
                    <input type="text" id="order_total" name="data[Order][order_total]" class="form-control" placeholder="Total">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-5 control-label text-center-mobile margin-bottom-10-mobile" for="truck_id">Type de camion souhaité :</label>
            <div class="col-md-5">
                <select id="truck_id" name="data[Order][truck_id]" class="select-chosen select-button" data-placeholder="Type de camion souhaité" style="width: 250px;">
                <option value=""></option>
                    <?php
                    foreach($trucks as $tId => $tName) {
                        ?>
                        <option value="<?php echo $tId; ?>"><?php echo $tName; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div style="font-style:italic; width: 100%; margin: auto; text-align: center;">
                <label class="col-md-12 control-label text-center margin-top-20 margin-bottom-20" for="example-clickable-pc" style="font-style:italic; width: 100%; margin: auto; text-align: center;">Le dispatch se réserve le droit de changer de type de camion s'il ne convient pas au chargement</label>
            </div>
            <div>
                <label class="col-md-6 control-label">Retours déblais : </label>
                <div class="col-md-6 text-left">
                <label class="switch switch-success"><input type="checkbox" id="cuttings_return" name="data[Order][cuttings_return]" ><span></span></label>
                </div>
            </div>
            <div class="form-group" id ="pop_up" style="display:none">
                <label style="font-size:20px;"><strong>Valorisable?</strong></label>
                <div>
                    <label class="switch switch-success"><input type="checkbox" id="switch-dap" ><span></span></label>
                    <div id ="upload-dap" style="margin-left: 25px; margin-top: 0px; display:none">
                        <input type="file" style="font-size:11px;" id="dap_file" name="data[Order][dap_file]" />
                    </div>
                </div>     
                
                <div>
                    <button class="valid_btn" style="background-color: #de815c;" id="cancel_pop_btn"><strong>Annuler<strong></button>
                    <button class="valid_btn" style= "background-color: #18673f;"id ="valid_pop_btn"><strong>Valider<strong></button>
                </div>     
            </div>
        </div>
    </div>
