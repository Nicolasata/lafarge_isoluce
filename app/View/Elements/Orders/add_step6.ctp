

    <div class="row no-margin-row">
        <div class="row margin-top-20 margin-bottom-20">   
            <div class="col-xs-12 text-center">
                <h4><?php echo ('Votre commande est enregistrée et transmise au dispatch.'); ?></h4>
            </div>
        </div>
            
        <div class="row text-center  margin-bottom-10">
            <div class="col-xs-12">      
                <?php
                echo $this->Html->link(
                    ('Passer une nouvelle commande'), 
                    array(
                        'controller' => 'orders',
                        'action' => 'add'
                    ), 
                    array(
                        'escape' => false,
                        'class' => 'btn btn-large btn-specific btn-effect-ripple'
                    )
                );
                ?>  
            </div>
        </div>
    </div>