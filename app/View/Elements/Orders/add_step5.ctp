
    <div class="row no-margin-row">

        <div style="font-style:italic; width: 100%; margin: auto; text-align: center;">
            <label class="col-md-12 control-label text-center margin-top-20 margin-bottom-20" for="example-clickable-pc" style="font-style:italic; width: 100%; margin: auto; text-align: center;"><strong>Récapitulatif de commande :</strong> </label>
        </div>

        <div class="form-group  text-center width-100 ">

            <strong><i class="fa fa-road"></i> Chantier</strong>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    Chantier :
                </div>
                <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                    <span id="confirm_site_name"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    Adresse :
                </div>
                <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                    <span id="confirm_site_addr"></span>
                </div>
            </div>
            <!--<div class="row">
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    Latitude :
                </div>
                <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                    <span id="confirm_site_lat"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    Longitude :
                </div>
                <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                    <span id="confirm_site_lng"></span>
                </div>
            </div>-->
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    Offre de prix liée :
                </div>
                <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                    <span id="confirm_order_price_linked"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    N° bon commande :
                </div>
                <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                    <span id="confirm_order_number"></span>
                </div>
            </div>            
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    Bon de commande :
                </div>
                <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                    <span id="confirm_order_file"></span>
                </div>
            </div>
            <div class="row margin-bottom-20">
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    Plan d'accès :
                </div>
                <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                    <span id="confirm_site_file_access"></span>
                </div>
            </div>

            <strong><i class="fa fa-map-marker"></i> Livraison : </strong>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    Date :
                </div>
                <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                    <span id="confirm_order_date"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    Plage horaire :
                </div>
                <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                    <span id="confirm_delivery_range"></span>
                </div>
            </div>
            <div class="row margin-bottom-20">
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    Commentaires :
                </div>
                <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                    <span id="confirm_delivery_comments"></span>
                </div>
            </div>            

            <strong><i class="fa fa-truck"></i> Camion :</strong>
            <div class="block-confirm-camions-rotation">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                        Nombre de camions :
                    </div>
                    <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                        <span id="confirm_order_truck_number"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    Volume journalier :
                    </div>
                    <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                        <span id="confirm_order_truck_volume_day"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    Camion souhaité :
                </div>
                <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                    <span id="confirm_truck"></span>
                </div>
            </div>
            <div class="row margin-bottom-20">
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">
                    Retours déblais :
                </div>
                <div class="col-xs-12 col-sm-8 text-left padding-left-0">
                    <span id="confirm_cuttings_return"></span>
                </div>
            </div>

            <strong><i class="fa fa-shopping-cart"></i> Produits</strong>
            <div class="row margin-bottom-20 block-products-result">
            </div>

            <strong><i class="fa fa-comment"></i> Commentaires</strong>
            <div class="row margin-bottom-20">
                <div class="col-xs-10 col-xs-offset-1">
                    <textarea id="order_comments" name="data[Order][order_comments]" rows="7" class="form-control width-100 margin-top-5" placeholder="Remplissez ce champs si vous avez une demande spécifique avant de valider votre commande"></textarea>
                </div>
            </div>

            <div style="font-style:italic; width: 100%; margin: auto; text-align: center;">
                <label class="col-md-12 control-label text-center margin-top-20 margin-bottom-20" style="font-style:italic; width: 100%; margin: auto; text-align: center;">Vous recevrez un email afin de confirmer la prise en charge de votre commande.</label>
            </div>
        </div>

    </div>