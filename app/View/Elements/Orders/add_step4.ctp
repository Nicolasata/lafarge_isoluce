
    <div class="row no-margin-row">
    
        <div class="form-group  text-center width-100" style="border-bottom: 1px solid #d8d7d7;padding: 10px 0;margin: 10px 0 20px;">
            <div class="col-xs-12 col-sm-4 text-left">
                Qté/Jour :
            </div>
            <div class="col-xs-12 col-sm-8 text-left">
                Produit :
            </div>
        </div>

        <div class="block-products">
            <div class="no-product text-center">
                <i>Aucun produit ajouté</i>
            </div>
        </div>

        <div class="form-group text-center width-100 block-product-add" style="border-top: 1px solid #d8d7d7;padding: 10px 10px 10px 0;margin: 20px 0 20px;">
            <div class="col-xs-12 col-sm-4 margin-bottom-10-mobile">
                <input type="text" id="product-quantity" class="form-control" placeholder="Ex: 18T">
            </div>
            <div class="col-xs-12 col-sm-7 margin-bottom-10-mobile">
                <input type="text" id="product-label" class="form-control" placeholder="Ex: Gravier">
            </div>
            <div class="col-xs-12 col-sm-1">
                <div id="product-add" class="btn btn-sm btn-success width-100px" style="height: 30px; font-size: 17px; line-height: 17px;"><i class="fa fa-plus"></i></div>
            </div>
        </div>

        <div style="font-style:italic; width: 100%; margin: auto; text-align: center;">
            <label class="col-md-12 control-label text-center margin-bottom-20 margin-top-20" for="example-clickable-pc" style="font-style:italic; width: 100%; margin: auto; text-align: center;">Pour information, voici une carte avec l'emplacement des carrières et les produits dont elle dispose :</label>
        </div>
        <div class="form-group">
            <div class="col-md-12"> 
                <div id="map_step4"></div>
            </div>
        </div>

        <input type="hidden" id="order_products" name="data[Order][Products]" value="" />

    </div>
    
    <script>
                
        var mapInfoStep4 = {};
        var displayInfoWindow4 = undefined;
        var mapStep4;
        var markerStep4;
        var markersStep4 = {};

        jQuery(function($){

            mapInfoStep4.id = 'map_step4';
            mapInfoStep4.zoom = 7;
            mapInfoStep4.lat = 44.40592613121;
            mapInfoStep4.lng = 5.13545968749;

        });

    </script>
