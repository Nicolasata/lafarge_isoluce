
    <div class="form-group" style="border-bottom: 1px dotted #ebeef2; padding: 0px 0px 15px;">
        <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified clickable-steps">
                <li class="<?php echo $step == 1 ? 'active' : ''; ?> add-title-link" data-step="1">
                    <a class="step-number" href="javascript:void(0)">
                        1
                    </a>
                    <a href="javascript:void(0)" style="pointer-events: none;cursor: none;">
                        <i class="fa fa-road"></i> <strong>Chantier</strong>
                    </a>
                </li>
                <li class="<?php echo $step == 2 ? 'active' : ''; ?> add-title-link disabled" data-step="2">
                    <a class="step-number" href="javascript:void(0)">
                        2
                    </a>
                    <a href="javascript:void(0)" style="pointer-events: none;cursor: none;">
                        <i class="fa fa-map-marker"></i> <strong>Livraison</strong>
                    </a>
                </li>
                <li class="<?php echo $step == 3 ? 'active' : ''; ?> add-title-link disabled" data-step="3">
                    <a class="step-number" href="javascript:void(0)">
                        3
                    </a>
                    <a href="javascript:void(0)" style="pointer-events: none;cursor: none;">
                        <i class="fa fa-truck"></i> <strong>Camion</strong>
                    </a>
                </li>
                <li class="<?php echo $step == 4 ? 'active' : ''; ?> add-title-link disabled" data-step="4">
                    <a class="step-number" href="javascript:void(0)">
                        4
                    </a>
                    <a href="javascript:void(0)" style="pointer-events: none;cursor: none;">
                        <i class="fa fa-shopping-cart"></i> <strong>Produits</strong>
                    </a>
                </li>
                <li class="<?php echo $step >= 5 ? 'active' : ''; ?> add-title-link disabled" data-step="5">
                    <a class="step-number" href="javascript:void(0)">
                        5
                    </a>
                    <a href="javascript:void(0)" style="pointer-events: none;cursor: none;">
                        <i class="fa fa-check"></i> <strong>Confirmation</strong>
                    </a>
                </li>
            </ul>
        </div>
    </div>