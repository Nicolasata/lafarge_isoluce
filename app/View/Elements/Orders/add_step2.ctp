         

    <div class="row no-margin-row">
                        
        <div class="form-group">
            <label class="col-md-4 control-label" for="order_date">Date :</label>
            <div class="col-md-6">
                <input type="text" id="order_date" name="data[Order][order_date]" class="form-control" data-min-date="10/07/19" placeholder="jj/mm/aaaa">
            </div>
        </div>
        <div class="form-group"><label class="col-md-12 control-label text-center" style="font-style:italic; width: 100%;margin: auto; text-align: center;">Sélectionner une ou plusieurs plage de livraisons :</label>
            <label class="col-md-4 control-label"></label>
            <div class="col-xs-12 col-md-6 col-md-offset-5">
                <div class="">
                    <div class="checkbox">
                        <label for="delivery_range_1">
                            <input type="checkbox" id="delivery_range_1" class="delivery_range" name="data[Order][delivery_range][]" value="sans préférence"> Peu importe
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="delivery_range_2">
                            <input type="checkbox" id="delivery_range_2" class="delivery_range" name="data[Order][delivery_range][]" value="ROTATION"> Rotation journée
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="delivery_range_3">
                            <input type="checkbox" id="delivery_range_3" class="delivery_range" name="data[Order][delivery_range][]" value="1ere heure"> 1ère Heure
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="delivery_range_4">
                            <input type="checkbox" id="delivery_range_4" class="delivery_range" name="data[Order][delivery_range][]" value="8h-10h"> Entre 8h et 10h
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="delivery_range_5">
                            <input type="checkbox" id="delivery_range_5" class="delivery_range" name="data[Order][delivery_range][]" value="10h-12h"> Entre 10h et 12h
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="delivery_range_6">
                            <input type="checkbox" id="delivery_range_6" class="delivery_range" name="data[Order][delivery_range][]" value="13h-15h"> Entre 13h et 15h
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="delivery_range_7">
                            <input type="checkbox" id="delivery_range_7" class="delivery_range" name="data[Order][delivery_range][]" value="15h-17h"> Entre 15h et 17h
                        </label>
                    </div>
                </div>
            </div>
            <label class="col-md-12 control-label text-center margin-top-20" style="font-style:italic; width: 100%;margin: auto; text-align: center;">Les commandes passées après 16H sont susceptibles de ne pas être honorées le lendemain.</label>
            
        </div>
        <div class="form-group block-info-heure-ouverture" style="display:none">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1">
                    <div class="alert alert-info text-center">
                        Merci de préciser l'heure d'ouverture du chantier ci-dessous dans les commentaires libres.
                    </div>
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="delivery_comments">Commentaires libre :</label>
            <div class="row margin-bottom-20">
                <div class="col-xs-10 col-xs-offset-1">
                    <textarea id="delivery_comments" name="data[Order][delivery_comments]" rows="4" class="form-control width-100 margin-top-5" placeholder="Vous pouvez ajouter un commentaire concernant la livraison"></textarea>
                </div>
            </div>
        </div>

    </div>
         

    <script>

    jQuery(function($){

        var date = new Date();
        date.setDate(date.getDate() + 1);

        $("#order_date").datepicker({
            language: 'fr',
            format: 'dd/mm/yyyy',
            startDate: date
        });
    });

    </script>