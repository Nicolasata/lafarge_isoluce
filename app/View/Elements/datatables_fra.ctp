<script type="text/javascript">
    var dataTableProcessing = '<?php echo __('Chargement...'); ?>';
    var dataTableSearch = '<?php echo __('Rechercher'); ?> :';
    var dataTableLengthMenu = '<?php echo __('Montrer _MENU_ éléments'); ?>';
    var dataTableInfo = '<?php echo __('Montrer _START_ à _END_ sur _TOTAL_ éléments'); ?>';
    var dataTableInfoEmpty = '<?php echo __('Aucun élément'); ?>';
    var dataTableLoading = '<?php echo __('Chargement...'); ?>';
    var dataTableZeroRecords = '<?php echo __('Aucun résultat'); ?>';
    var dataTableEmpty = '<?php echo __('Aucune donnée à afficher'); ?>';
    var dataTableFirst = '<?php echo __('Premier'); ?>';
    var dataTablePrevious = '<?php echo __('Précédent'); ?>';
    var dataTableNext = '<?php echo __('Suivant'); ?>';
    var dataTableLast = '<?php echo __('Dernier'); ?>';
</script>
