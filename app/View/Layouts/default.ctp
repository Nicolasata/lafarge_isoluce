<?php
echo $this->Html->docType('html5'); ?>
<!-- [if IE 9]><html class="no-js lt-ie10"><![endif] -->
<!-- [if gt IE 9]><! -->


<html>
<head>
	<?php echo $this->Html->charset(); ?>

            <meta http-equiv="X-UA-compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <title><?php echo $this->fetch('title'); ?> - <?php echo Configure::read('Common.name') ?></title>
    
    
       	<meta http-equiv="content-language" content="fr_FR" />
	<meta name="language" content="fr" />
	<meta name="description" content="..."/>

       
        <?php
		echo $this->Html->meta(
			'favicon.ico',
			'img/favicon.png',
			array('type' => 'icon')
		);
	?>
        
         <?php echo $this->element('analyticstracking'); ?>

        <?php echo $this->Html->script(array('vendor/modernizr-2.8.3.min', 'vendor/jquery-3.2.1.min', 'vendor/bootstrap.min')); ?>
        <?php echo $this->Html->css(array('bootstrap.min' , 'plugins' , 'main' , 'specific' , 'themes')); ?>
        
    </head>
    <body>
        <!-- Full Background -->
        <!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->
        <img src="<?php echo Router::url('/', true); ?>img/login2_full_bg.jpg" alt="Full Background" class="full-bg animation-pulseSlow">
        <!-- END Full Background -->

  
    <?php echo $this->fetch('content'); ?>       

    <?php echo $this->Flash->render(); ?>

<!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
        <?php echo $this->Html->script(array('plugins', 'app')); ?>
    </body>
</html>
