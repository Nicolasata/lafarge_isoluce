<?php
echo $this->Html->docType('html5'); ?>
<!-- [if IE 9]><html class="no-js lt-ie10"><![endif] -->
<!-- [if gt IE 9]><! -->


<html>
<head>
	<?php echo $this->Html->charset(); ?>

            <meta http-equiv="X-UA-compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <title><?php echo $this->fetch('title'); ?> - <?php echo Configure::read('Common.name') ?></title>
    
    
       	<meta http-equiv="content-language" content="fr_FR" />
	<meta name="language" content="fr" />
	<meta name="description" content="..."/>

       
        <?php
		echo $this->Html->meta(
			'favicon.ico',
			'img/favicon.png',
			array('type' => 'icon')
		);
	?>
        
         <?php echo $this->element('analyticstracking'); ?>

         <?php echo $this->Html->script(array('vendor/modernizr-2.8.3.min', 'vendor/jquery-3.2.1.min', 'vendor/bootstrap.min')); ?>
        <?php echo $this->Html->css(array('bootstrap.min' , 'plugins' , 'main' , 'specific' , 'themes')); ?>
        
    </head>
    <body>
  
        
                <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <div class="preloader-spinner themed-background-specific hidden-lt-ie10"></div>
                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <!-- END Preloader -->

            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
               
                <!-- Main Container -->
                <div id="main-container" style="margin-left: 0px;">
                    <header class="navbar navbar-inverse navbar-fixed-top" style="left: 0px;">
                        <ul class="nav navbar-nav-custom">
                            <li class="animation-fadeInQuick">
                                <img src="<?php echo Router::url('/', true); ?>img/logo_lafarge.png" style="height: 50px;">
                            </li>
                                                        
                            <li class="animation-fadeInQuick" style="margin-left:30px;">
                                <a href="<?php echo Router::url('/', true); ?>supervisors/export" class="text-dark"><strong><i class='fa fa-download'></i> Export</strong></a>
                            </li>                    
                            <li class="animation-fadeInQuick" style="margin-left:30px;">
                                <a href="<?php echo Router::url('/', true); ?>supervisors/imports" class="text-dark"><strong><i class='fa fa-upload'></i> Imports</strong></a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav-custom pull-right">
                                                        
                            <li class="hidden-xs animation-fadeInQuick">
                                <a href="#" class="text-dark"><strong><i class='fa fa-key'></i> <?php echo $this->Session->read('Supervisor.username'); ?></strong></a>
                            </li>
                            <li class="hidden-xs animation-fadeInQuick">
                                <a href="#" class="text-dark"><strong><i class='fa fa-user-secret'></i>  <?php echo $this->Session->read('Supervisor.name'); ?></strong></a>
                            </li>
                            
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo Router::url('/', true); ?>img/avatar.png" alt="avatar">
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="<?php echo Router::url('/', true); ?>supervisors/logout">
                                            <i class="fa fa-power-off fa-fw pull-right"></i>
                                            Deconnexion
                                        </a>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </header>
                    
                    
                    <div id="page-content">
                
                        <?php echo $this->fetch('content'); ?>       

                        <?php echo $this->Flash->render(); ?>
                        
                    </div>
                </div>
            </div>
        </div> 

        <?php echo $this->Html->script(array('plugins', 'app', 'pages/formsWizard', 'pages/formsComponents',  'pages/formsValidation')); ?>
    </body>
</html>
