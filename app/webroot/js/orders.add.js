
var eventEnterPad = false;
var canAddMultiple = true;
var markerStep1 = undefined;

var mapLoaded = function() {

    //map - Step 1

    mapStep1 = new google.maps.Map(document.getElementById(mapInfoStep1.id), {
        zoom: mapInfoStep1.zoom,
        center: {
            lat: mapInfoStep1.lat,
            lng: mapInfoStep1.lng
        }
    });

    geocoder = new google.maps.Geocoder;
        
    var mapOptions = {
        gestureHandling: 'greedy',
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: false
    };
    mapStep1.setOptions(mapOptions);
    
    //Geolocation
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          
            $('#site_lat').val(position.coords.latitude);
            $('#site_lng').val(position.coords.longitude);

            var latlng = {lat: position.coords.latitude, lng: position.coords.longitude};
            mapStep1.panTo(latlng);
            mapStep1.setZoom(16);
            
            var markerData1 = {
                position: latlng,
                map: mapStep1,
                scrollwheel : false,
                streetViewControl : false,
                visibleScrollZoom: true,
                draggable: true,
                title: "Cliquez pour déplacer la position"
            };
            markerStep1 = new google.maps.Marker(markerData1);
            markerStep1.setVisible(true);
            markerStep1.addListener('dragend', markerMoved);
            
            geocoder.geocode({'location': latlng}, function(results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        $('#site_addr').val(results[0].formatted_address);
                    } else {
                        $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>Aucune adresse trouvée pour cette position.</p>', {
                            type: "danger",
                            delay: 10000,
                            allow_dismiss: true,
                            offset: {from: 'top', amount: 80}
                        });
                    }
                } else {
                    $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>Erreur lors de la récupération de l\'adresse de la nouvelle position.</p>', {
                        type: "danger",
                        delay: 10000,
                        allow_dismiss: true,
                        offset: {from: 'top', amount: 80}
                    });
                }
            });
        }, function() {
            console.error('Erreur récupération position');
        });
    } else {
        console.error('Navigateur');
    }

    //map - Step 4
    mapStep4 = new google.maps.Map(document.getElementById(mapInfoStep4.id), {
        zoom: mapInfoStep4.zoom,
        center: {
            lat: mapInfoStep4.lat,
            lng: mapInfoStep4.lng
        }
    });
        
    stonepits.forEach(function(stonepit) {
        stonepit = stonepit.Stonepit;
        
        var markerData4 = {
            position: {lat: parseFloat(stonepit.latitude), lng: parseFloat(stonepit.longitude)},
            map: mapStep4,
            stonepit: stonepit,
            scrollwheel : false,
            streetViewControl : false,
            visibleScrollZoom: true,
            draggable: false,
            icon: rootUrl+'img/icon-stonepit.png'
        };
        markerStep4 = new google.maps.Marker(markerData4);
        markerStep4.addListener('click', callbackClickMarker4);
        markerStep4.setVisible(true);
    });

};

var callbackClickMarker4 = function() {

	var marker = $(this)[0];
	
	//Centre la carte
	mapStep4.panTo(marker.getPosition());

	//Supprime l'ancienne info window
	if(displayInfoWindow4 != undefined) {
		displayInfoWindow4.close();
		displayInfoWindow4 = undefined;
	}

	//Affiche une info window
    var infoWindow = new google.maps.InfoWindow();
    
    var stonepit = marker.stonepit;
    var contentString = '';
    var contentString = '<div class="info-step4">';
    contentString += '<h4>'+stonepit.site_description+' ('+stonepit.site_code+')</h4>';
    contentString += '<p class="address">'+stonepit.address+','+ (stonepit.address_cpt != '' ? ''+stonepit.address_cpt+', ' : ' ')+stonepit.address_code+' '+stonepit.address_city+'</p>';
    contentString += '<p class="position">'+stonepit.latitude+', '+stonepit.longitude+' - <a href="tel:'+stonepit.phone.replace(/\s/g,'')+'" target="_blank">'+stonepit.phone+'</a></p>';
    contentString += '<p class="products"><b>Produits:</b><br />';
    marker.stonepit.products.split('-').forEach(function(p) {
        contentString += '<span class="product">'+p+'</span>';
    });
    contentString += '</p>';
    contentString += '</div>';

	infoWindow.setContent(contentString);
	infoWindow.marker = marker;
	displayInfoWindow4 = infoWindow;
	displayInfoWindow4.open(mapStep4, marker);

};

var markerMoved = function(event) {

    $('#site_lat').val(event.latLng.lat());
    $('#site_lng').val(event.latLng.lng());
    
    geocoder.geocode({'location': event.latLng}, function(results, status) {
        if (status === 'OK') {
            if (results[0]) {
                $('#site_addr').val(results[0].formatted_address);
            } else {
                $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>Aucune adresse trouvée pour cette position.</p>', {
                    type: "danger",
                    delay: 10000,
                    allow_dismiss: true,
                    offset: {from: 'top', amount: 80}
                });
            }
        } else {
            $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>Erreur lors de la récupération de l\'adresse de la nouvelle position.</p>', {
                type: "danger",
                delay: 10000,
                allow_dismiss: true,
                offset: {from: 'top', amount: 80}
            });
        }
    });

}

var showStep = undefined

$(document).ready(function() {
    
    var isRunning = false;

    var products = [];
    
    showStep(null, currentStep);

    $("#wizard-button-next").bind('click', function(event, data){

        if(!isRunning) {
            setRunning(true);

            //Do work
            if (currentStep === 1) {
                checkChantier();
            }
            else if (currentStep === 2) {
                checkLivraison();
            }
            else if (currentStep === 3) {
                checkCamion();
            }
            else if (currentStep === 4) {
                checkProduits();
            }
            else if (currentStep === 5) {
                checkConfirmation();
            }
        }
    });
        
    $("#wizard-button-previous").bind('click', function(event, data){
        if(!isRunning) {
            setRunning(true);
            
            if(currentStep == 6) {
                
                if($('#BoxOrderChoice').val() == 'ABANDON') {
                    showStep(6, 2);
                }
                else if(!needCustomsDeclaration) {
                    showStep(6, 4);
                }
                else {
                    previousStep();
                }
            }
            else {
                previousStep();
            }
        }
    });

    
    /******* EVENTS ********/
    
	$(document).on("change", "#site_addr", function(e) {
		var input = $(this);
		var address = input.val();

		if(address != '') {
		    geocoder.geocode({'address': address}, function(results, status) {

                if(markerStep1 != undefined) {
                    markerStep1.setMap(null);
                    //markerStep1.removeListener('dragend');
                    markerStep1 = undefined;
                } 

				if (status == 'OK') {
                    $('#site_lat').val(results[0].geometry.location.lat());
                    $('#site_lng').val(results[0].geometry.location.lng());
					mapStep1.panTo(results[0].geometry.location);
                    mapStep1.setZoom(16);
                    
                    var markerData = {
                        position: results[0].geometry.location,
                        map: mapStep1,
                        scrollwheel : false,
                        streetViewControl : false,
                        visibleScrollZoom: true,
                        draggable: true,
                        title: "Cliquez pour déplacer la position"
                    };
                    markerStep1 = new google.maps.Marker(markerData);
                    markerStep1.setVisible(true);
                    markerStep1.addListener('dragend', markerMoved);

				} else {
                    $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>'+i18n_position_find_invalid+'</p>', {
                        type: "danger",
                        delay: 10000,
                        allow_dismiss: true,
                        offset: {from: 'top', amount: 80}
                    });
				}
			});
		}
    });

	$(document).on("change", ".delivery_range", function(e) {
        var showBlockInfo = false;
        $('.delivery_range:checked').each(function() {
            
            if($(this).val() == 'ROTATION' || $(this).val() == '1ere heure') {
                showBlockInfo = true;
            }
        });
        if(showBlockInfo) {
            $('.block-info-heure-ouverture').show('fast');
        }
        else {
            $('.block-info-heure-ouverture').hide('fast');
        }
    });
        
	$(document).on("click", "#product-add", function(e) {	
        addProduct();	
    });
    
	$(document).on("click", ".product-remove", function(e) {	
        var btn = $(this);
        var item = btn.closest('.product-item');

        item.hide('fast');
        setTimeout(function() {
            item.remove();
            
            if($('.product-item').length == 0) {
                $('.block-products .no-product').show();
            }
        }, 200);
    });

    $(document).on("keypress", "body", function(e) {
        var keyId = e.keyCode;
        
        //Enter
        if(keyId == 13 && eventEnterPad) {
            e.preventDefault();
            e.stopPropagation();
            addProduct();	
            return false;
        }
    });

	$(document).on("click", ".add-title-link", function(e) {	
        var li = $(this);
        var stepChoose = li.data('step');

        if(!li.hasClass('.disabled')) {
            if(stepChoose < currentStep) {
                showStep(currentStep, stepChoose);
            }
        }
    });

    /******* STEPS ********/

    $(document).ready(function() {
        $("#switch-derogation").click(function(event){
            $("#upload-derogation").toggle(1000);
        });

        $("#cuttings_return").click(function(event){
            if( !($('#cuttings_return').is(':checked')) ){
                $("#pop_up").hide(1000);
                $("#dap_file").val(null);
            } else {
                $("#pop_up").toggle(1000);
            }
        });

        $("#switch-dap").click(function(event){ $("#upload-dap").toggle(1000); });
        $("#cancel_pop_btn").click(function(event){ $('#cuttings_return').trigger('click'); });
        $("#valid_pop_btn").click(function(event){ $("#pop_up").hide(1000); });            
    });

    function checkChantier() {
        
        if($('#site_name').val().trim() == '') { 
            $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>Vous devez ajouter un nom de chantier.</p>', {
                type: "danger",
                delay: 10000,
                allow_dismiss: true,
                offset: {from: 'top', amount: 80}
            });
            setRunning(false);
            return;
        }
        
        if($('#site_lat').val().trim() == '') { 
            $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>Vous devez sélectionner une adresse valide.</p>', {
                type: "danger",
                delay: 10000,
                allow_dismiss: true,
                offset: {from: 'top', amount: 80}
            });
            setRunning(false);
            return;
        }

        if(markerStep4 != undefined) {
            markerStep4.setMap(null);
            markerStep4 = undefined;
        } 

        var position = {lat: parseFloat($('#site_lat').val().trim()), lng: parseFloat($('#site_lng').val().trim())};
        mapStep4.setZoom(11);
        mapStep4.panTo(position);
        
        var markerData4 = {
            position: position,
            map: mapStep4,
            scrollwheel : false,
            streetViewControl : false,
            visibleScrollZoom: true,
            draggable: true,
            title: "Cliquez pour déplacer la position"
        };
        markerStep4 = new google.maps.Marker(markerData4);
        markerStep4.setVisible(true);
        
        nextStep();
    }
    
    function checkLivraison() {

        if($('#order_date').val().trim() == '') { 
            $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>Vous devez choisir une date de livraison.</p>', {
                type: "danger",
                delay: 10000,
                allow_dismiss: true,
                offset: {from: 'top', amount: 80}
            });
            setRunning(false);
            return;
        }
        
        if($('.delivery_range:checked').val() == undefined) { 
            $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>Vous devez sélectionner au moins une rotation.</p>', {
                type: "danger",
                delay: 10000,
                allow_dismiss: true,
                offset: {from: 'top', amount: 80}
            });
            setRunning(false);
            return;
        }
        
        var showBlockCamionRotation = false;
        $('.delivery_range:checked').each(function() {
            if($(this).val() == 'ROTATION') {
                showBlockCamionRotation = true;
            }
        });
        if(showBlockCamionRotation) {
            $('.block-camions-rotation').show();
        }
        else {
            $('.block-camions-rotation').hide();
        }
        
        nextStep();
    }
    
    function checkCamion() {

        if($('#truck_id').val() == "") { 
            $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>Vous devez choisir un camion souhaité.</p>', {
                type: "danger",
                delay: 10000,
                allow_dismiss: true,
                offset: {from: 'top', amount: 80}
            });
            setRunning(false);
            return;
        }
        
        $('.delivery_range:checked').each(function() {
            var regExp = /\(([^)]+)\)/;

            if($(this).val() == 'ROTATION') {
                var matches = regExp.exec($('#truck_id').find(":selected").text());
                var res = matches[0].split(' ')
                $("#product-quantity").val(res[0].replace('(', ' ').trim() + 'T');
            }
        });

        nextStep();
    }
    
    function checkProduits() {
                
        if($('.product-item').length == 0) {
            $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>Vous devez ajouter au moins un produit.</p>', {
                type: "danger",
                delay: 10000,
                allow_dismiss: true,
                offset: {from: 'top', amount: 80}
            });
            setRunning(false);
            return;
        }

        products = [];
        $('.product-item').each(function() {
            products.push({quantity: $(this).data('quantity'), label: $(this).data('label')});
        });

        $('#order_products').val(JSON.stringify(products));

        nextStep();
    }
   
    function checkConfirmation() {
        
        var form = $('#OrderAddForm');

        form.attr('action', rootUrl+'orders/add');
        form.submit();

    }

    /******* UTILS FUNCTION ********/

    function showStep(oldStep, newStep) {
        currentStep = newStep;
        
        eventEnterPad = false;

        if (currentStep === 1) {
            $('#wizard-button-previous').hide();
        }
        else if (currentStep === 2) {
            $('.title-date').hide();
            setTimeout(function() {
                $('#wizard-button-previous').show();
            }, 500);
        }
        else if (currentStep === 3) {
            $('.title-date-value').text($('#order_date').val());
            $('.title-date').show();
            setTimeout(function() {
                $('#wizard-button-previous').show();
            }, 500);
        }
        else if (currentStep === 4) {
            eventEnterPad = true;

            canAddMultiple = true
            $('.delivery_range:checked').each(function() {
                if($(this).val() == 'ROTATION') {
                    canAddMultiple = false;
                }
            });
            if(!canAddMultiple && $('.product-item').length > 0) {
                $('.block-product-add').hide('fast');
            }
            else {
                $('.block-product-add').show('fast');
            }

            setTimeout(function() {
                $('#wizard-button-previous').show();
            }, 500);
        }
        else if (currentStep === 5) {
            setTimeout(function() {
                $('#wizard-button-previous').show();
            }, 500);
            
            $('#confirm_site_name').text($('#site_name').val());
            $('#confirm_site_addr').text($('#site_addr').val());
            $('#confirm_order_number').text($('#order_number').val());
            $('#confirm_order_price_linked').text($('#order_price_linked').val());
            $('#confirm_order_date').text($('#order_date').val());

            $('#confirm_name_contact_site').text($('#name_contact_site').val());
            $('#confirm_firstname_contact_site').text($('#firstname_contact_site').val());
            $('#confirm_phonenbr_contact_site').text($('#phonenbr_contact_site').val());
            $('#confirm_risk_comments').text($('#risk_comments').val());

            $('#confirm_derogation_file').text(($('#derogation_file').val() != "") ? 'Fichier présent' : 'Aucun fichier');
            $('#confirm_dap_file').text(($('#dap_file').val() != "") ? 'Fichier présent' : 'Aucun fichier');
            $('#confirm_order_file').text(($('#order_file').val() != "") ? 'Fichier présent' : 'Aucun fichier');
            $('#confirm_site_file_access').text(($('#site_file_access').val() != "") ? 'Fichier présent' : 'Aucun fichier');

            var strDeliveryRange = '';
            $('.delivery_range:checked').each(function() {
                if(strDeliveryRange == '') {
                    strDeliveryRange = $(this).val();
                }
                else {
                    strDeliveryRange = strDeliveryRange.concat(', ' + $(this).val());
                }
            });
            $('#confirm_delivery_range').text(strDeliveryRange);
            $('#confirm_truck').text($('#truck_id option:selected').text());
            $('#confirm_delivery_comments').text($('#delivery_comments').val());    
            
            var showConfirmCamionRotation = false;
            $('.delivery_range:checked').each(function() {
                if($(this).val() == 'ROTATION') {
                    showConfirmCamionRotation = true;
                }
            });
            if(showConfirmCamionRotation) {
                $('.block-confirm-camions-rotation').show();
                $('#confirm_order_truck_number').text($('#order_truck_number').val());
                $('#confirm_order_truck_volume_day').text($('#order_truck_volume_day').val());
                $('#confirm_order_total').text($('#order_total').val());
            }
            else {
                $('.block-confirm-camions-rotation').hide();
            }      
            
            $('#confirm_cuttings_return').text(($('#cuttings_return').prop('checked')) ? 'Oui' : 'Non');
            
           
            $('.block-products-result').empty();
            for(var i = 0; i < products.length; i++) {
                var product = products[i];            

                var html = '';
                html = html.concat('<div class="col-xs-12 col-sm-3 col-sm-offset-1 text-left">Commande '+(i+1)+' :</div>');
                html = html.concat('<div class="col-xs-12 col-sm-8 text-left padding-left-0">'+product.quantity+' '+product.label+'</div>');
                $('.block-products-result').append(html);
            }

        }
        else if (currentStep === 6) {
            $('#wizard-button-next-text').parent().hide();
            $('#wizard-button-previous').hide();
        }

        for(var i = 1; i <= 5; i++) {
            if(i > currentStep) {
                $('.add-title-link[data-step='+i+']').addClass('disabled');
            }
            else {
                $('.add-title-link[data-step='+i+']').removeClass('disabled');
            }
        }

        if(oldStep != null) {
            $("#step-"+(oldStep)).fadeOut("fast", function() {
                $("#step-"+newStep).fadeIn("slow");
                setRunning(false);
            });
        }
        else {
            $(".step").fadeOut("fast")
            $("#step-"+newStep).fadeIn("slow");
            setRunning(false);
        }
    }

    function setRunning(state) {
        if(state) {
            isRunning = true;
            $("#wizard-button-next").prop('disabled', true);
            $("#wizard-button-previous").prop('disabled', true);
        }
        else {
            isRunning = false;
            $("#wizard-button-next").prop('disabled', false);
            $("#wizard-button-previous").prop('disabled', false);
        }
    }

    function nextStep() {
        if(currentStep != $('.step').length) {
            showStep(currentStep, currentStep+1);
        }
        else {
            setRunning(false);
        }
    }
    
    function previousStep() {
        if(currentStep != 1) {
            showStep(currentStep, currentStep-1);
        }
        else {
            setRunning(false);
        }
    }

    function addProduct() {
        if($('#product-quantity').val().trim() == '') { 
            $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>Vous devez ajouter une quantité.</p>', {
                type: "danger",
                delay: 10000,
                allow_dismiss: true,
                offset: {from: 'top', amount: 80}
            });
            setRunning(false);
            return;
        }
		
        if($('#product-label').val().trim() == '') { 
            $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>Vous devez ajouter un nom de produit.</p>', {
                type: "danger",
                delay: 10000,
                allow_dismiss: true,
                offset: {from: 'top', amount: 80}
            });
            setRunning(false);
            return;
        }
        
        $('.block-products .no-product').hide();

        var quantity = $('#product-quantity').val();
        var label = $('#product-label').val();    
        $('#product-quantity').val('');
        $('#product-label').val('');
        
        var html = '';
        html = html.concat('<div class="product-item row" data-quantity="'+quantity+'" data-label="'+label+'" style="margin: 0;padding: 15px 10px 15px 0;">');
        html = html.concat('    <div class="col-xs-12 col-sm-4 text-left product-item-quantity">'+quantity+'</div>');
        html = html.concat('    <div class="col-xs-12 col-sm-7 text-left product-item-label">'+label+'</div>');
        html = html.concat('    <div class="col-xs-12 col-sm-1">');
        html = html.concat('        <button class="btn btn-sm btn-warning product-remove" style="margin-top: -5px;"><i class="fa fa-trash"></i></button>');
        html = html.concat('    </div>');
        html = html.concat('</div>');

        $('.block-products').append(html);

        if(!canAddMultiple && $('.product-item').length > 0) {
            $('.block-product-add').hide('fast');
        }
        else {
            $('.block-product-add').show('fast');
        }
    }

});

