<?php
App::uses('AppController', 'Controller');

class ProfilesController extends AppController {
	
	function beforeFilter() {
		parent::beforeFilter();

		$this->layout = 'profil';
	}
    
	public function index() {

		$user = $this->checkConnectedUser();
		
		$this->loadModel('UserProfile');
		$profiles = $this->UserProfile->find('all', array(
			'conditions' => array(
				'UserProfile.user_id' => $user['id'],
				'UserProfile.archived' => null,
			),
			'order' => 'firstname',
			'recursive' => -1
		));

		$this->set('profiles', $profiles);

	}
	  
    public function add() {
        
		$user = $this->checkConnectedUser();		
		
		if ($this->request->is(array('post', 'put'))) {

            try {
                
				$data = $this->requestData();

				
				if(empty($data['lastname'])) {
					$this->Flash->error(__('Le nom est obligatoire.'));
					return;
				}
				
				if(empty($data['firstname'])) {
					$this->Flash->error(__('Le prénom est obligatoire.'));
					return;
				}
				
				if(empty($data['email'])) {
					$this->Flash->error(__('L\'email est obligatoire.'));
					return;
				}
				
				$now = new DateTime("now", new DateTimeZone('UTC'));

				$newUser = array(
					'user_id' => $user['id'],
					'lastname' => $data['lastname'],
					'firstname' => $data['firstname'],
					'email' => $data['email'],
					'phone' => (!empty($data['phone']) ? $data['phone'] : null),
					'work' => (!empty($data['work']) ? $data['work'] : null),
					'created' => $now->format('Y-m-d H:i:s'),
					'modified' => $now->format('Y-m-d H:i:s')
				);

				$this->loadModel('UserProfile');
				$this->UserProfile->create();
				if($this->UserProfile->save(array('UserProfile' => $newUser))) {
					
					$this->Flash->success(__('Profil ajouté.'));
					
					return $this->redirect(
						array('controller' => 'profiles', 'action' => 'index')
					);

				}
				else {
					$this->Flash->error(__('Une erreur est survenue, merci de contacter un administrateur.'));
					return;
				}                          
				
            } catch (Exception $ex) {
                $this->Flash->error($ex->getMessage());
                $this->logError($ex->getMessage());
                return;
			}  
		}
	}
	
	public function select($userProfileId = null) {

		$user = $this->checkConnectedUser();

		if($userProfileId != null) {		

			$this->loadModel('UserProfile');
			$profile = $this->UserProfile->find('first', array(
				'conditions' => array(
					'UserProfile.id' => $userProfileId,
					'UserProfile.archived' => null,
				),
				'recursive' => -1
			));
				
			if($profile) {
				if($profile['UserProfile']['user_id'] == $user['id']) {

					$this->Session->write('Profile', $profile['UserProfile']);

					return $this->redirect(
						array('controller' => 'orders', 'action' => 'add')
					);
					
				}
				else {
					$this->Flash->error(__('Une erreur est survenue, merci de contacter un administrateur.'));
					return $this->redirect(
						array('controller' => 'profiles', 'action' => 'index')
					);
				}
			}
			else {
				$this->Flash->error(__('Une erreur est survenue, merci de contacter un administrateur.'));
				return $this->redirect(
					array('controller' => 'profiles', 'action' => 'index')
				);
			}
		}
		else {
			$this->Flash->error(__('Une erreur est survenue, merci de contacter un administrateur.'));
			return $this->redirect(
				array('controller' => 'profiles', 'action' => 'index')
			);
		}

	}
	
	public function remove($userProfileId = null) {

		$user = $this->checkConnectedUser();

		if($userProfileId != null) {		

			$this->loadModel('UserProfile');
			$profile = $this->UserProfile->find('first', array(
				'conditions' => array(
					'UserProfile.id' => $userProfileId,
					'UserProfile.archived' => null,
				),
				'recursive' => -1
			));
				
			if($profile) {
				if($profile['UserProfile']['user_id'] == $user['id']) {

					$this->Session->delete('Profile');

					$now = new DateTime("now", new DateTimeZone('UTC'));
					
					$profile = $this->UserProfile->find('first', array(
						'conditions' => array(
							'UserProfile.id' => $userProfileId,
							'UserProfile.archived' => null,
						),
						'recursive' => -1
					));

					$saveUser = array(
						'id' => $profile['UserProfile']['id'],
						'archived' => $now->format('Y-m-d H:i:s'),
						'modified' => $now->format('Y-m-d H:i:s')
					);

					if($this->UserProfile->save(array('UserProfile' => $saveUser))) {
						$this->Flash->success(__('Profil archivé.'));
					}
					else {
						$this->Flash->error(__('Une erreur est survenue, merci de contacter un administrateur.'));						
					}     
				}
				else {
					$this->Flash->error(__('Une erreur est survenue, merci de contacter un administrateur.'));
				}
			}
			else {
				$this->Flash->error(__('Une erreur est survenue, merci de contacter un administrateur.'));
				return $this->redirect(
					array('controller' => 'profiles', 'action' => 'index')
				);
			}
		}
		else {
			$this->Flash->error(__('Une erreur est survenue, merci de contacter un administrateur.'));
			return $this->redirect(
				array('controller' => 'profiles', 'action' => 'index')
			);
		}

		return $this->redirect(
			array('controller' => 'profiles', 'action' => 'index')
		);

	}	
   
}
