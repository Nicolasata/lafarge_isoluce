<?php
App::uses('AppController', 'Controller');

App::uses('TemplatesEmail', 'Vendor/Constante');

class UsersController extends AppController {
	
	function beforeFilter() {
		parent::beforeFilter();

	}
    
	public function login() {

		$this->checkDisconnectedUser();
		
        $this->layout = 'default';
        
		if ($this->request->is(array('post', 'put'))) {
            try {
                $data = $this->requestData();
                				
				//Find user
                $user = $this->User->find('first', array(
                    'conditions' => array(
                        'User.number' => $data['number'],
                        'User.archived' => null
                    ),
                    'recursive' => -1
				));

                if($user != null) {                    
                    $pwd = Security::hash($data['password'], null, true);                    
                    if($pwd === $user['User']['password']) {

                        $this->setUser($user['User']); 
                        return $this->redirectLogin();
                    }
                    else {
                        $this->Flash->error(__('Mot de passe incorrect.'));
                        return;
                    }	
                }
                else {
                    $this->Flash->error(__('Ce compte n\'existe pas.'));
                    return;
                }
				
            } catch (Exception $ex) {
                $this->Flash->error($ex->getMessage());
                $this->logError($ex->getMessage());
                return;
            }  

        }

    }
    
	public function logout() {
        $this->logAction('Logout');
        $this->Session->delete('User');
        $this->Session->delete('Profile');
        return $this->redirectLogin();
    }
	  
    public function forgotPassword() {

        $this->checkDisconnectedUser();

        $this->layout = 'default';
        
		if ($this->request->is(array('post', 'put'))) {

            try {
                
                $data = $this->requestData();
				
                //Find user
                $this->loadModel('User');
                $user = $this->User->find('first', array(
                    'conditions' => array(
                        'User.email' => $data['email']
                    ),
                    'recursive' => -1
                ));
                
				if(count($user)) {

                    //Link verification email
                    $sha1 = Security::hash($user['User']['id'].'+'.$user['User']['created'], 'sha1', true);
                    
				    $rootUrl = str_replace('http://', '', str_replace('https://', '', Router::url('/', true)));
                    $url = $rootUrl.'recover-password?i='.$user['User']['id'].'&h='.$sha1;
        
                    //Send email
                    try {
                        $attr = array(
                            'USER_ID' => $user['User']['id'],
                            'USER_NUMBER' => $user['User']['number'],
                            'USER_EMAIL' => $user['User']['email'],
                            'RECOVER_URL' => $url
                        );

                        $this->loadModel('Message');
                        $this->Message->addEmailAndSend(
                            TemplatesEmail::RECOVER_PASSWORD_FR, 
                            $user['User']['email'], 
                            $attr,
                            $user['User']['id']
                        );
                    }
                    catch(Exception $e) {
                        $this->logError($e->getMessage());
                    }
                    
                    $this->Flash->success('Email envoyé.');
                    return $this->redirectLogin();
						
                }
                else {
                    $this->Flash->error(__('Ce compte n\'existe pas.'));
                    return;
                }
				
            } catch (Exception $ex) {
                $this->Flash->error($ex->getMessage());
                $this->logError($ex->getMessage());
                return;
            }  

        }

    }
    
    public function recoverPassword() {
        
		$this->checkDisconnectedUser();

        $this->layout = 'default';

		if ($this->request->is(array('post', 'put'))) {

            try {
                
                $data = $this->requestData();
     
                $id = $data['id'];
                $hash = $data['hash'];

                $this->set('i', $id);
                $this->set('h', $hash);
                        
                $user = $this->User->find('first' , array(
                    'conditions' => array(
                        'User.id' => $id
                    ),
                    'recursive' => -1
                ));
                if($user != null) {

                    $sha1 = Security::hash($user['User']['id'].'+'.$user['User']['created'], 'sha1', true);
				
                    if($sha1 == $hash) {
                        
                        $testPwd = $this->User->checkPwd($data['pwd1'], $data['pwd2']);
                        if($testPwd != null) {
                            $this->Flash->error($testPwd);
                            return;
                        } 
        
                        $now = new DateTime("now", new DateTimeZone('UTC'));

                        $saveUser = array(
                            'id' => $user['User']['id'],
                            'password' => Security::hash($data['pwd1'], null, true),
                            'modified' => $now->format('Y-m-d H:i:s')
                        );

                        if($this->User->save(array('User' => $saveUser))) {
                            $this->setUser($user['User']);  
                            
                            $this->Flash->success(__('Mot de passe mis à jour.'));
                            return $this->redirectLogin();
                        }
                        else {
                            $this->Flash->error(__('Une erreur est survenue, merci de contacter un administrateur.'));
                            return;
                        }                
					}
					else {
                        $this->Flash->error(__('Une erreur est survenue, merci de contacter un administrateur.'));
                        return;
					}
				}
				else {
                    $this->Flash->error(__('Ce compte n\'existe pas.'));
                    return;                    
                }
                
				
            } catch (Exception $ex) {
                $this->Flash->error($ex->getMessage());
                $this->logError($ex->getMessage());
                return;
            }  

        }
        else {
            $error = '';
    
            $id = $this->request->query['i'];
            $hash = $this->request->query['h'];
    
            $user = $this->User->find('first' , array(
                'conditions' => array(
                    'User.id' => $id
                ),
                'recursive' => -1
            ));
            if($user != null) {
                
                $sha1 = Security::hash($user['User']['id'].'+'.$user['User']['created'], 'sha1', true);
                    
                if($sha1 == $hash) {
                    $this->set('i', $id);
                    $this->set('h', $hash);
                }
                else {
                    $error = __('Une erreur est survenue, merci de contacter un administrateur.');
                }
            }
            else {
                $error = __('Une erreur est survenue, merci de contacter un administrateur.');
            }
    
            $this->set('error', $error);
        }

    }
  
	// Supervisors
   
	public function supervisor_index() {

		$this->checkConnectedSupervisor();

		$this->layout = 'supervisor';

	}
   
}
