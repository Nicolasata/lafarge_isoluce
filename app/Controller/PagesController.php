<?php
App::uses('AppController', 'Controller');

App::uses('TemplatesEmail', 'Vendor/Constante');

class PagesController extends AppController {
	
	function beforeFilter() {
		parent::beforeFilter();

	}

	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
   
		$result = array('hasError' => false);  
		if ($this->request->is('post')) {  
			try {

				$data = $this->requestData();

				$attr = array(
					'TEAM' => $this->Session->read('Team.name'),
					'USER_ID' => $this->Session->read('Team.User.id'),
					'USER_NAME' => $this->Session->read('Team.User.name'),
					'USER_TEXT' => $data['msg'],
					'USER_EMAIL' => $this->Session->read('Team.User.email')
				);

				$this->loadModel('Message');
				$this->Message->addEmail(
					TemplatesEmail::CONTACT, 
					Configure::read('Common.mail_contact'), 
					$attr,
					$this->Session->read('Team.User.id')
				);

				$result['msg'] = __('Merci pour votre message. Nous y répondrons au plus vite !');

			} catch (Exception $ex) {
				$result['hasError'] = true;
				$result['error'] = $ex->getMessage();
				$this->logError($ex->getMessage());
			}                    
		}
		else {
			$result['hasError'] = true;
			$result['error'] = __('Méthode non autorisée pour cette action');
		}
		return $this->responseJSON($result);
	}
}
