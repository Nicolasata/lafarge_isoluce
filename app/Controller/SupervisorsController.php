<?php

App::uses('AppController', 'Controller');

class SupervisorsController extends AppController {
	
    public $helpers = array('Csv'); 

	function beforeFilter() {
		parent::beforeFilter();
    }      
          
	public function index() {
        
		$this->checkDisconnectedSupervisor();
        
        $this->layout = 'default';
        
		if ($this->request->is(array('post', 'put'))) {
            try {
                $data = $this->requestData();

                
                $supervisor = $this->Supervisor->find('first', array(
                    'conditions' => array(
                        'Supervisor.username' => $data['username']
                    ),
                    'recursive' => -1
                ));
                
                if(count($supervisor) && $supervisor['Supervisor']['id'] != null) {

                    $pwd = Security::hash($data['password'], null, true);
                    if($pwd === $supervisor['Supervisor']['password']) {

                        $this->setSupervisor($supervisor['Supervisor']); 
                        return $this->redirect(Router::url('/', true).'supervisors/export/'); 

                    }
                    else {
                        $this->Flash->error( __('Erreur de connexion'));
                        return;
                    }	
                }
                else {
                    $this->Flash->error( __('Erreur de connexion'));
                    return;
                }		
				
            } catch (Exception $ex) {
                $this->Flash->error($ex->getMessage());
                $this->logError($ex->getMessage());
                return;
            }  

        }

    }
    
	public function logout() {

		$this->checkConnectedSupervisor();

        $this->Session->delete('Supervisor');
		
        return $this->redirect(Router::url('/', true)); 
		
    }
    
	public function export() {
        
		$this->checkConnectedSupervisor();

        $this->layout = 'supervisor';

        $error = null;

        
		if ($this->request->is(array('post', 'put'))) {
            try {
                $data = $this->requestData();

                $dateStart = \DateTime::createFromFormat('d/m/Y', $data['Export']['date_start'], new \DateTimeZone('UTC'));
                $dateEnd = \DateTime::createFromFormat('d/m/Y', $data['Export']['date_end'], new \DateTimeZone('UTC'));

                $this->loadModel('Order');
                $orders = $this->Order->find('all', array(
                    'contain' => array('UserProfile', 'Truck', 'OrderProduct'),
                    'conditions' => array(
                        'Order.created >=' => $dateStart->format('Y-m-d').' 00:00:00',
                        'Order.created <=' => $dateEnd->format('Y-m-d').' 23:59:59'
                    ),
                    'order' => 'Order.created'
                ));
                
                //Generate file
                
                $date = new DateTime();
                $filename = 'orders_export_'.$date->format('Ymd_His'); 
                
                $this->set("filename", $filename);
                $this->set("orders", $orders);
                $this->layout = null;
                $this->autoLayout = false;
                $this->render('export_csv');
                
				
            } catch (Exception $ex) {
                $error = $ex->getMessage();
            }  

        }

        $this->set('error', $error);

    }
    
    
	public function imports() {
        
		$this->checkConnectedSupervisor();

        $this->layout = 'supervisor';

        $errorTrucks = null;
        $errorClients = null;
        
		if ($this->request->is(array('post', 'put'))) {
            try {
                $data = $this->requestData();

                //Trucks
                if(isset($data['type']) && $data['type'] == 'trucks') {

                    if($data['Import']['file_trucks']['size'] > 0) {
                        $tmp_name = $data['Import']['file_trucks']['tmp_name'];
                        
			            require_once APP.'Vendor'.DS.'PhpOffice.php';
                        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($tmp_name);
                        $worksheet = $spreadsheet->getActiveSheet();
                        $lines = $worksheet->toArray();

                        $headers = array_shift($lines);

                        if($headers[0] == 'famille_vehicule') {

                            $trucks = array();
                            foreach($lines as $line) {
                                $truck = array();
                                foreach($headers as $hId => $hName) {
                                    $truck[$hName] = $line[$hId];
                                }
                                array_push($trucks, $truck); 
                            }
                            
                            if(count($trucks)) {
    
                                $this->loadModel('Truck');
                                $trucksBdd = $this->Truck->find('list', array(
                                    'fields'=> array('id', 'famille_vehicule'),
                                    'recursive' => -1
                                ));
    
                                $truckToAdd = array();
                                $truckToUpdate = array();
                                $truckToDisactive = array();
                                foreach($trucks as $truck) {
                                    $pos = array_search($truck['famille_vehicule'].'', $trucksBdd, true);
                                    
                                    //Create
                                    if($pos === false) {
                                        array_push($truckToAdd, $truck);
                                    }
                                    //Update
                                    else {
                                        $truck['id'] = $pos;
                                        array_push($truckToUpdate, $truck);
                                        unset($trucksBdd[$pos]);
                                    }
                                }
                                $truckToDisactive = $trucksBdd;
    
                                $now = new \DateTime('now', new \DateTimeZone('UTC'));
    
                                //Add
                                if(count($truckToAdd)) {
                                    foreach($truckToAdd as $t){
                                        $newT = array(
                                            'famille_vehicule' => $t['famille_vehicule'],
                                            'nom_famille_vehicule' => $t['nom_famille_vehicule'],
                                            'libelle_court' => $t['libelle_court'],
                                            'nom_reg_comptable' => $t['nom_reg_comptable'],
                                            'tons' => $t['Tonnes'],
                                            'created' => $now->format('Y-m-d H:i:s'),
                                            'modified' => $now->format('Y-m-d H:i:s')
                                        );
                                        $this->Truck->create();
                                        $this->Truck->save(array('Truck' => $newT));
    
                                    }
                                }
                                
                                //Update
                                if(count($truckToUpdate)) {
                                    foreach($truckToUpdate as $t){
                                        $updateT = array(
                                            'id' => $t['id'],
                                            'famille_vehicule' => $t['famille_vehicule'],
                                            'nom_famille_vehicule' => $t['nom_famille_vehicule'],
                                            'libelle_court' => $t['libelle_court'],
                                            'nom_reg_comptable' => $t['nom_reg_comptable'],
                                            'tons' => $t['Tonnes'],
                                            'modified' => $now->format('Y-m-d H:i:s')
                                        );
                                        $this->Truck->save(array('Truck' => $updateT));
                                    }
                                }
                                
                                //Update
                                if(count($truckToDisactive)) {
                                    foreach($truckToDisactive as $tId => $tFamille){
                                        $updateT = array(
                                            'id' => $tId,
                                            'archived' => $now->format('Y-m-d H:i:s'),
                                            'modified' => $now->format('Y-m-d H:i:s')
                                        );
                                        $this->Truck->save(array('Truck' => $updateT));
                                    }
                                }
    
                                $this->Flash->success('L\'import est effectué.');
    
                            }
                            else {
                                $errorTrucks = 'Aucune ligne trouvé dans le fichier.';
                            }   
                        }
                        else {
                            $errorTrucks = 'La colonne famille_vehicule n\'a pas été trouvé.';
                        }  
                     
                    }

                }
                //Clients
                else if(isset($data['type']) && $data['type'] == 'clients') {

                    if($data['Import']['file_clients']['size'] > 0) {
                        $tmp_name = $data['Import']['file_clients']['tmp_name'];
                        
			            require_once APP.'Vendor'.DS.'PhpOffice.php';
                        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($tmp_name);
                        $worksheet = $spreadsheet->getActiveSheet();
                        $lines = $worksheet->toArray();

                        $headers = array_shift($lines);


                        if($headers[0] == 'compte_client') {

                            $clients = array();
                            foreach($lines as $line) {
                                $client = array();
                                foreach($headers as $hId => $hName) {
                                    $client[$hName] = $line[$hId];
                                }
                                array_push($clients, $client); 
                            }
                            
                            if(count($clients)) {
    
                                $this->loadModel('User');
                                $clientsBdd = $this->User->find('list', array(
                                    'fields'=> array('id', 'number'),
                                    'recursive' => -1
                                ));
    
                                $clientToAdd = array();
                                $clientToUpdate = array();
                                $clientToDisactive = array();
                                foreach($clients as $client) {
                                    $pos = array_search($client['compte_client'].'', $clientsBdd, true);
                                    
                                    //Create
                                    if($pos === false) {
                                        array_push($clientToAdd, $client);
                                    }
                                    //Update
                                    else {
                                        $client['id'] = $pos;
                                        array_push($clientToUpdate, $client);
                                        unset($clientsBdd[$pos]);
                                    }
                                }
                                $clientToDisactive = $clientsBdd;
    
                                $now = new \DateTime('now', new \DateTimeZone('UTC'));
    
                                //Add
                                if(count($clientToAdd)) {
                                    foreach($clientToAdd as $t){
                                        $newC = array(
                                            'number' => $t['compte_client'],
                                            'raison_sociale_compte_client' => $t['raison_sociale_compte_client'],
                                            'nom_sect_commercial' => $t['nom_sect_commercial'],
                                            'email' => (!empty($t['email']) ? $t['email'] : null),
                                            'nom_commercial' => (!empty($t['nom_commercial']) ? $t['nom_commercial'] : null),
                                            'prenom_commercial' => (!empty($t['prenom_commercial']) ? $t['prenom_commercial'] : null),
                                            'email_commercial' => (!empty($t['email_commercial']) ? $t['email_commercial'] : null),
                                            'phone_commercial' => (!empty($t['phone_commercial']) ? $t['phone_commercial'] : null),
                                            'created' => $now->format('Y-m-d H:i:s'),
                                            'modified' => $now->format('Y-m-d H:i:s')
                                        );
                                        $this->User->create();
                                        $this->User->save(array('User' => $newC));
    
                                    }
                                }
                                
                                //Update
                                if(count($clientToUpdate)) {
                                    foreach($clientToUpdate as $t){
                                        $updateC = array(
                                            'id' => $t['id'],
                                            'number' => $t['compte_client'],
                                            'raison_sociale_compte_client' => $t['raison_sociale_compte_client'],
                                            'nom_sect_commercial' => $t['nom_sect_commercial'],
                                            'email' => (!empty($t['email']) ? $t['email'] : null),
                                            'nom_commercial' => (!empty($t['nom_commercial']) ? $t['nom_commercial'] : null),
                                            'prenom_commercial' => (!empty($t['prenom_commercial']) ? $t['prenom_commercial'] : null),
                                            'email_commercial' => (!empty($t['email_commercial']) ? $t['email_commercial'] : null),
                                            'phone_commercial' => (!empty($t['phone_commercial']) ? $t['phone_commercial'] : null),
                                            'modified' => $now->format('Y-m-d H:i:s')
                                        );
                                        $this->User->save(array('User' => $updateC));
                                    }
                                }
                                
                                //Update
                                /*if(count($clientToDisactive)) {
                                    foreach($clientToDisactive as $cId => $cNumber){
                                        $updateC = array(
                                            'id' => $cId,
                                            'archived' => $now->format('Y-m-d H:i:s'),
                                            'modified' => $now->format('Y-m-d H:i:s')
                                        );
                                       $this->User->save(array('User' => $updateC));
                                    }
                                }*/
    
                                $this->Flash->success('L\'import est effectué.');
    
                            }
                            else {
                                $errorClients = 'Aucune ligne trouvé dans le fichier.';
                            }   
                        }
                        else {
                            $errorClients = 'La colonne famille_vehicule n\'a pas été trouvé.';
                        }  
                    }

                }
                else {
                    $this->Flash->error('Une erreur est survenue. Merci de réessayer.');
                }                
				
            } catch (Exception $ex) {
                $this->Flash->error($ex->getMessage());
            }  

        }

        $this->set('errorTrucks', $errorTrucks);
        $this->set('errorClients', $errorClients);

    }
}
