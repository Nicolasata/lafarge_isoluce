<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
        //public $scaffold = 'admin';
	
	public $components = array(
        //'DebugKit.Toolbar',
        'Flash',
        'Session',
        'Datatable'
    );
        
	public $helpers = array(
	    'Html', 
		'Form', 
		'Session'
	);

	function beforeFilter() {

    }

    function redirectLogin()  {
        if($this->Session->check('User')) {            
			$this->redirect(
				array('controller' => 'profiles', 'action' => 'index')
			);
        }
        else {
            $this->redirect(
                array('controller' => 'users', 'action' => 'login')
            );
        }
    }
    
    function checkConnectedUser($ajax = false, $flash = true) {
        if($this->Session->check('User')) {
            return $this->Session->read('User');
        }

        if($ajax) {
            return null;
        }
        else {
            if($flash) {
                $this->Flash->error(__('Vous devez être connecté pour accéder à cette zone.'));
            }
            return $this->redirectLogin();
        }
    }
    
    function checkDisconnectedUser() {
        if(!$this->Session->check('User')) {
            return true;
        }        
        $this->Flash->error(__('Vous devez être déconnecté pour accéder à cette zone.'));
        return $this->redirectLogin();
    }
    
    function formatUserObj($user) {

        $currentUser = array(
            'id' => $user['id'],
            'number' => $user['number'],
            'email' => $user['email'],
            'nom_commercial' => $user['nom_commercial'],
            'prenom_commercial' => $user['prenom_commercial'],
            'email_commercial' => $user['email_commercial'],
            'phone_commercial' => $user['phone_commercial']
        );
        return $currentUser;
    }
    
    function setUser($user) {

        $currentUser = $this->formatUserObj($user);
        $this->Session->write('User', $currentUser);

    }
    
    function checkConnectedSupervisor($ajax = false, $flash = true) {
        if($this->Session->check('Supervisor')) {
            return $this->Session->read('Supervisor');
        }

        if($ajax) {
            return null;
        }
        else {
            if($flash) {
                $this->Flash->error(__('Vous devez être connecté pour accéder à cette zone.'));
            }
            return $this->redirectLogin();
        }
    }
    
    function formatSupervisorObj($supervisor) {

        $currentSupervisor = array(
            'id' => $supervisor['id'],
            'username' => $supervisor['username'],
            'name' => $supervisor['name']
        );
        return $currentSupervisor;
    }
    
    function setSupervisor($supervisor) {

        $currentSupervisor = $this->formatSupervisorObj($supervisor);
        $this->Session->write('Supervisor', $currentSupervisor);

    }

    function checkDisconnectedSupervisor() {
        if(!$this->Session->check('Supervisor')) {
            return true;
        }        
        return  $this->redirect(
            array('controller' => 'supervisors', 'action' => 'export')
        );
    }
           
	function logAction() {
        try {    
            $this->log('Action', 'info');
        } catch (Exception $e) {
            $this->log('logInfo: '.$e->getMessage(), 'error');
        }
    }

	function logInfo($msg) {
        try {    
            $this->log($msg, 'info');
        } catch (Exception $e) {
            $this->log('logInfo: '.$e->getMessage(), 'error');
        }
    }

	function logWarn($msg) {
        try {    
            $this->log($msg , 'warning');
        } catch (Exception $e) {
            $this->log('logWarn: '.$e->getMessage(), 'error');
        }
    }

	function logError($msg) {
        try {    
            $this->log($msg , 'error');
        } catch (Exception $e) {
            $this->log('logError: '.$e->getMessage(), 'error');
        }
    }
    
    function requestData() {
        return $this->request->data ;
    }
        
	function responseJson($result) {
        $this->response->type('json');
        $this->set("result", htmlspecialchars(json_encode($result), ENT_NOQUOTES));
        $this->layout = "ajax";
        $this->render('/Elements/ajax');
	}
		
	function responseDatatable() {
        $this->DataTable->mDataProp = true;
        $this->response->type('json');
        $this->set("result", htmlspecialchars(json_encode($this->DataTable->getResponse()), ENT_NOQUOTES));
        $this->layout = "ajax";
        $this->render('/Elements/ajax');
	}
	            

}
