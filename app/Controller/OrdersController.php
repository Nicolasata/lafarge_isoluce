<?php
App::uses('AppController', 'Controller');

App::uses('TemplatesEmail', 'Vendor/Constante');

class OrdersController extends AppController {
	
	function beforeFilter() {
		parent::beforeFilter();

        $this->layout = 'order';
	}
    	  
    public function add() {

		/*$this->loadModel('Message');
		$messages = $this->Message->findToSend();
		foreach($messages as $message) {
			$this->Message->sent($message);
		}*/
        
		$user = $this->checkConnectedUser();

		if(null == $this->Session->read('Profile')) {
			$this->Flash->error(__('Aucun profil n\'est sélectionné.'));			
			return $this->redirect(
				array('controller' => 'profiles', 'action' => 'index')
			);
		}

		$finish = false;
		
		$this->loadModel('Truck');
		$trucksBrut = $this->Truck->find('all', array(
			'fields' => array('id', 'nom_famille_vehicule', 'tons'),
			'conditions' => array(
				'Truck.archived' => null
			),
			'recursive' => -1
		));
		$trucks = array();
		foreach($trucksBrut as $t) {
			$trucks[$t['Truck']['id']] = $t['Truck']['nom_famille_vehicule'].' ('.$t['Truck']['tons'].' tonnes)';
		}
		
		$this->loadModel('Stonepit');
		$stonepits = $this->Stonepit->find('all', array(
			'recursive' => -1
		));
		
		if ($this->request->is(array('post', 'put'))) {

			$profile = $this->Session->read('Profile');

			$data = $this->requestData();

			$now = new \DateTime('now', new \DateTimeZone('UTC'));
			$orderDate = \DateTime::createFromFormat('d/m/Y', $data['Order']['order_date'], new \DateTimeZone('UTC'));

			$newOrders = array(
				'user_id' => $user['id'],
				'user_profile_id' => $profile['id'],
				'site_name' => $data['Order']['site_name'],
				'site_addr' => $data['Order']['site_addr'],
				'site_lat' => $data['Order']['site_lat'],
				'site_lng' => $data['Order']['site_lng'],
				'order_number' => $data['Order']['order_number'],
				'order_price_linked' => $data['Order']['order_price_linked'],
				'order_date' => $orderDate->format('Y-m-d'),
				'delivery_range' => json_encode($data['Order']['delivery_range']),
				'delivery_comments' => $data['Order']['delivery_comments'],
				'order_truck_number' => (isset($data['Order']['order_truck_number']) && !empty($data['Order']['order_truck_number'])) ? $data['Order']['order_truck_number'] : null,
				'order_truck_volume_day' => (isset($data['Order']['order_truck_volume_day']) && !empty($data['Order']['order_truck_volume_day'])) ? $data['Order']['order_truck_volume_day'] : null,
				'truck_id' => $data['Order']['truck_id'],
				'cuttings_return' => (isset($data['Order']['cuttings_return']) && $data['Order']['cuttings_return'] == 'on' ? 1: 0),
				'order_comments' => $data['Order']['order_comments'],
				'created' => $now->format('Y-m-d H:i:s'),
				'modified' => $now->format('Y-m-d H:i:s'),
				
				'name_contact_site' => $data['Order']['name_contact_site'],
				'firstname_contact_site' => $data['Order']['name_contact_site'],
				'phonenbr_contact_site' => $data['Order']['phonenbr_contact_site'],
				'risk_comments' => $data['Order']['risk_comments'],
				'order_total' => $data['Order']['order_total']
			);

			$dirFile = WWW_ROOT.'files'.DS;
			$pathFile = '/files/';

			if($data['Order']['order_file']['size'] > 0) {
				$key = 'order_file';
				$tmp_name = $_FILES['data']['tmp_name']['Order'][$key];
				$fileFilename = $user['id'].'_'.$profile['id'].'_order_'.uniqid().'_'.date('YmdHis').'.'.pathinfo($_FILES['data']['name']['Order'][$key], PATHINFO_EXTENSION);
				move_uploaded_file($tmp_name, $dirFile.$fileFilename);
				$newOrders['order_file'] = $pathFile.$fileFilename;
			}
			
			if($data['Order']['site_file_access']['size'] > 0) {
				$key = 'site_file_access';
				$tmp_name = $_FILES['data']['tmp_name']['Order'][$key];
				$fileFilename = $user['id'].'_'.$profile['id'].'_site_access_'.uniqid().'_'.date('YmdHis').'.'.pathinfo($_FILES['data']['name']['Order'][$key], PATHINFO_EXTENSION);
				move_uploaded_file($tmp_name, $dirFile.$fileFilename);
				$newOrders['site_file_access'] = $pathFile.$fileFilename;
			}

			if($data['Order']['dap_file']['size'] > 0) {
				$key = 'dap_file';
				$tmp_name = $_FILES['data']['tmp_name']['Order'][$key];
				$fileFilename = $user['id'].'_'.$profile['id'].'_site_access_'.uniqid().'_'.date('YmdHis').'.'.pathinfo($_FILES['data']['name']['Order'][$key], PATHINFO_EXTENSION);
				move_uploaded_file($tmp_name, $dirFile.$fileFilename);
				$newOrders['dap_file'] = $pathFile.$fileFilename;
			}

			if($data['Order']['derogation_file']['size'] > 0) {
				$key = 'derogation_file';
				$tmp_name = $_FILES['data']['tmp_name']['Order'][$key];
				$fileFilename = $user['id'].'_'.$profile['id'].'_site_access_'.uniqid().'_'.date('YmdHis').'.'.pathinfo($_FILES['data']['name']['Order'][$key], PATHINFO_EXTENSION);
				move_uploaded_file($tmp_name, $dirFile.$fileFilename);
				$newOrders['derogation_file'] = $pathFile.$fileFilename;
			}
			
			$this->Order->create();
			$createdOrder = $this->Order->save(array('Order' => $newOrders));
			$orderId = $createdOrder['Order']['id'];
			
			$products = json_decode($data['Order']['Products'], true);
			$this->loadModel('OrderProduct');
			foreach($products as $product) {
				$newProducts = array(
					'order_id' => $orderId,
					'quantity' => $product['quantity'],
					'label' => $product['label'],
					'created' => $now->format('Y-m-d H:i:s'),
					'modified' => $now->format('Y-m-d H:i:s')
				);

				$this->OrderProduct->create();
				$this->OrderProduct->save(array('OrderProduct' => $newProducts));
			}

			//Send email
			try {

				$rootUrl = str_replace('http://', '', str_replace('https://', '', Router::url('/', true)));
				
				$sha1 = Security::hash($orderId.'+'.$newOrders['created'], 'sha1', true);
				$urlAccepted = $rootUrl.'orders/accepted?o='.$orderId.'&h='.$sha1;
				$urlUpdated = $rootUrl.'orders/updated?o='.$orderId.'&h='.$sha1;

				$urlAccepted = str_replace('http://', '', str_replace('https://', '', $urlAccepted));
				$urlUpdated = str_replace('http://', '', str_replace('https://', '', $urlUpdated));

				$productsHtml = '';
				foreach($products as $product) {
					$productsHtml.= $product['quantity'].' '.$product['label'].'<br />';
				}

				$attr = array(
					'USER_ID' => $user['id'],
					'USER_NUMBER' => $user['number'],
					'USER_EMAIL' => $profile['email'],
					'ACCEPTED_URL' => $urlAccepted,
					'UPDATED_URL' => $urlUpdated,
					'SITE_NAME' => $newOrders['site_name'],
					'SITE_ADDR' => $newOrders['site_addr'],
					'SITE_LAT' => $newOrders['site_lat'],
					'SITE_LNG' => $newOrders['site_lng'],
					'SITE_FILE_ACCESS' => ((isset($newOrders['site_file_access']) && $newOrders['site_file_access'] != null) ? $rootUrl.$newOrders['site_file_access'] : ''),
					'ORDER_NUMBER' => $newOrders['order_number'],
					'ORDER_PRICE_LINKED' => $newOrders['order_price_linked'],
					'ORDER_DATE' => $newOrders['order_date'],
					'ORDER_FILE' => ((isset($newOrders['order_file']) && $newOrders['order_file'] != null) ? $rootUrl.$newOrders['order_file'] : ''),
					'DELIVERY_RANGE' => implode(', ', json_decode($newOrders['delivery_range'], true)),
					'ORDER_TRUCK_NUMBER' => ($newOrders['order_truck_number']) ? $newOrders['order_truck_number'] : '',	
					'ORDER_TRUCK_VOLUME_DAY' => ($newOrders['order_truck_volume_day']) ? $newOrders['order_truck_volume_day'] : '',		
					'DELIVERY_COMMENTS' => $newOrders['delivery_comments'],		
					'TRUCK' => $trucks[$newOrders['truck_id']],
					'CUTTINGS_RETURN' => ($newOrders['cuttings_return'] == 1) ? 'Oui' : 'Non',
					'ORDER_COMMENTS' => $newOrders['order_comments'],			
					'CREATED' => $newOrders['created'],
					'PRODUCTS' => $productsHtml,

					'NAME_CONTACT_SITE' => $newOrders['name_contact_site'],
					'FIRSTNAME_CONTACT_SITE' => $newOrders['firstname_contact_site'],
					'DEROGATION_FILE' => ((isset($newOrders['derogation_file']) && $newOrders['derogation_file'] != null) ? $rootUrl.$newOrders['derogation_file'] : ''),
					'PHONENBR_CONTACT_SITE' => $newOrders['phonenbr_contact_site'],
					'RISK_COMMENTS' => $newOrders['risk_comments'],
					'ORDER_TOTAL' => $newOrders['order_total'],
					'DAP_FILE' => ((isset($newOrders['dap_file']) && $newOrders['dap_file'] != null) ? $rootUrl.$newOrders['dap_file'] : '')
				);
				
				$this->loadModel('Message');
				$this->Message->addEmailAndSend(
					TemplatesEmail::DISPATCH, 
					Configure::read('Common.dispatch'), 
					$attr,
					$user['id']
				);

				$updateOrder = array(
					'id' => $orderId,
					'send' => $now->format('Y-m-d H:i:s')
				);
				$this->Order->save(array('Order' => $updateOrder));
			}
			catch(Exception $e) {
				$this->logError($e->getMessage());
			}

			$finish = true;

		}

		$this->set('trucks', $trucks);
		$this->set('stonepits', $stonepits);
		$this->set('finish', $finish);
        
	}

    public function accepted() {

		$this->layout = 'default';
		
		$error = null;
        
		$id = $this->request->query['o'];
		$hash = $this->request->query['h'];

		$order = $this->Order->find('first' , array(
			'conditions' => array(
				'Order.id' => $id,
				'Order.validated' => null,
				'Order.refused' => null
			)
		));
		if($order != null) {
			
			$sha1 = Security::hash($order['Order']['id'].'+'.$order['Order']['created'], 'sha1', true);
				
			if($sha1 == $hash) {

				$productsHtml = '';
				foreach($order['OrderProduct'] as $product) {
					$productsHtml.= $product['quantity'].' '.$product['label'].'<br />';
				}

				$clientContact = ($order['User']['prenom_commercial']) ? $order['User']['prenom_commercial'] : '';
				$clientContact.= ' '.(($order['User']['nom_commercial']) ? $order['User']['nom_commercial'] : '');
				$clientContact.= ' - '.(($order['User']['email_commercial']) ? $order['User']['email_commercial'] : '');
				$clientContact.= ' '.(($order['User']['phone_commercial']) ? $order['User']['phone_commercial'] : '');
				$clientContact = trim($clientContact);
				
				$attr = array(
					'USER_NUMBER' => $order['User']['number'],
					'USER_EMAIL' => $order['UserProfile']['email'],
					'CLIENT_CONTACT' => $clientContact,
					'SITE_NAME' => $order['Order']['site_name'],
					'SITE_ADDR' => $order['Order']['site_addr'],
					'SITE_LAT' => $order['Order']['site_lat'],
					'SITE_LNG' => $order['Order']['site_lng'],
					'ORDER_NUMBER' => $order['Order']['order_number'],		
					'ORDER_PRICE_LINKED' => $order['Order']['order_price_linked'],			
					'ORDER_DATE' => $order['Order']['order_date'],
					'DELIVERY_RANGE' => implode(', ', json_decode($order['Order']['delivery_range'], true)),
					'DELIVERY_COMMENTS' => $order['Order']['delivery_comments'],	
					'ORDER_TRUCK_NUMBER' => ($order['Order']['order_truck_number']) ? $order['Order']['order_truck_number'] : '',	
					'ORDER_TRUCK_VOLUME_DAY' => ($order['Order']['order_truck_volume_day']) ? $order['Order']['order_truck_volume_day'] : '',		
					'TRUCK' => $order['Truck']['nom_famille_vehicule'],
					'CUTTINGS_RETURN' => ($order['Order']['cuttings_return'] == 1) ? 'Oui' : 'Non',
					'ORDER_COMMENTS' => $order['Order']['order_comments'],
					'CREATED' => $order['Order']['created'],
					'PRODUCTS' => $productsHtml,

					'NAME_CONTACT_SITE' => $newOrders['name_contact_site'],
					'FIRSTNAME_CONTACT_SITE' => $newOrders['firstname_contact_site'],
					'PHONENBR_CONTACT_SITE' => $newOrders['phonenbr_contact_site'],
					'RISK_COMMENTS' => $newOrders['risk_comments'],
					'ORDER_TOTAL' => $newOrders['order_total']
				);

				$this->loadModel('Message');
				$this->Message->addEmailAndSend(
					TemplatesEmail::DISPATCH_ACCEPTED, 
					$order['UserProfile']['email'], 
					$attr
				);

				$now = new \DateTime('now', new \DateTimeZone('UTC'));
				$updateOrder = array(
					'id' => $order['Order']['id'],
					'validated' => $now->format('Y-m-d H:i:s')
				);
				$this->Order->save(array('Order' => $updateOrder));
				
			}
			else {
				$error = __('Une erreur est survenue, merci de contacter un administrateur.');
			}
		}
		else {
			$error = __('Une erreur est survenue, merci de contacter un administrateur.');
		}

		$this->set('error', $error);
    }

    public function updated() {

		$this->layout = 'default';
		
		$error = null;
        
		$id = $this->request->query['o'];
		$hash = $this->request->query['h'];

		$order = $this->Order->find('first' , array(
			'conditions' => array(
				'Order.id' => $id,
				'Order.validated' => null,
				'Order.refused' => null
			)
		));
		if($order != null) {
			
			$sha1 = Security::hash($order['Order']['id'].'+'.$order['Order']['created'], 'sha1', true);
				
			if($sha1 == $hash) {

				$productsHtml = '';
				foreach($order['OrderProduct'] as $product) {
					$productsHtml.= $product['quantity'].' '.$product['label'].'<br />';
				}
				
				$attr = array(
					'SITE_NAME' => $order['Order']['site_name'],
					'SITE_ADDR' => $order['Order']['site_addr'],
					'SITE_LAT' => $order['Order']['site_lat'],
					'SITE_LNG' => $order['Order']['site_lng'],
					'ORDER_NUMBER' => $order['Order']['order_number'],	
					'ORDER_PRICE_LINKED' => $order['Order']['order_price_linked'],				
					'ORDER_DATE' => $order['Order']['order_date'],
					'DELIVERY_RANGE' => implode(', ', json_decode($order['Order']['delivery_range'], true)),
					'DELIVERY_COMMENTS' => $order['Order']['delivery_comments'],	
					'ORDER_TRUCK_NUMBER' => ($order['Order']['order_truck_number']) ? $order['Order']['order_truck_number'] : '',	
					'ORDER_TRUCK_VOLUME_DAY' => ($order['Order']['order_truck_volume_day']) ? $order['Order']['order_truck_volume_day'] : '',		
					'TRUCK' => $order['Truck']['nom_famille_vehicule'],
					'CUTTINGS_RETURN' => ($order['Order']['cuttings_return'] == 1) ? 'Oui' : 'Non',
					'ORDER_COMMENTS' => $order['Order']['order_comments'],
					'CREATED' => $order['Order']['created'],
					'PRODUCTS' => $productsHtml,

					'NAME_CONTACT_SITE' => $newOrders['name_contact_site'],
					'FIRSTNAME_CONTACT_SITE' => $newOrders['firstname_contact_site'],
					'PHONENBR_CONTACT_SITE' => $newOrders['phonenbr_contact_site'],
					'RISK_COMMENTS' => $newOrders['risk_comments'],
					'ORDER_TOTAL' => $newOrders['order_total'],
				);

				$this->loadModel('Message');
				$this->Message->addEmailAndSend(
					TemplatesEmail::DISPATCH_UPDATED, 
					$order['UserProfile']['email'], 
					$attr
				);

				$now = new \DateTime('now', new \DateTimeZone('UTC'));
				$updateOrder = array(
					'id' => $order['Order']['id'],
					'refused' => $now->format('Y-m-d H:i:s')
				);
				$this->Order->save(array('Order' => $updateOrder));
				
			}
			else {
				$error = __('Une erreur est survenue, merci de contacter un administrateur.');
			}
		}
		else {
			$error = __('Une erreur est survenue, merci de contacter un administrateur.');
		}

		$this->set('error', $error);
    }
   
}
