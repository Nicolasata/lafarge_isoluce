
ALTER TABLE `orders` 
ADD COLUMN `order_truck_number` INT UNSIGNED NULL AFTER `delivery_comments`,
ADD COLUMN `order_truck_volume_day` INT UNSIGNED NULL AFTER `order_truck_number`;

ALTER TABLE `orders` 
CHANGE COLUMN `order_truck_volume_day` `order_truck_volume_day` VARCHAR(50) NULL DEFAULT NULL ;
